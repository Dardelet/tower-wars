package attack;

import java.util.ArrayList;
import java.util.Timer;
import serveur.ErrorResponseFormatException;
import serveur.Server;
import structures.CurrentPlayer;
import structures.Player;
import structures.Tower;
import structures.TowerList;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

public class AttackActivity extends Activity implements OnClickListener
{
	private Button attackButton;
	private Button returnButton;
	private ViewGroup blocs;
	private ViewGroup logs;
	private TextView health;
	private int xp;
	private ProgressBar mProgress;
	private String tower;
	private String previousTower;
	private int oldLifePoints;
	private LinearLayout.LayoutParams param;
	private ArrayList<View> list;
	private int timeAttack = 5;
	private String idTour;
	private Timer timer = new Timer();


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		CurrentPlayer.load(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attack);

		param = new LinearLayout.LayoutParams (150, 150);
		param.gravity = Gravity.CENTER;
		mProgress = (ProgressBar) findViewById(R.id.LL);
		blocs = (ViewGroup) findViewById(R.id.Ablocs);
		logs = (ViewGroup) findViewById(R.id.Alogs);
		health = (TextView) findViewById(R.id.life_points);
		attackButton = (Button) findViewById(R.id.Aattack);
		returnButton = (Button) findViewById(R.id.Areturn);

		attackButton.setOnClickListener(this);
		returnButton.setOnClickListener(this);
		if(CurrentPlayer.lifePoints == 0)
		attackButton.setVisibility(View.INVISIBLE);
		tower = TowerList.towerAttack.getTower();
		idTour = Integer.toString(TowerList.towerAttack.getId());
		draw();
		updateLifeBar();
	}

	private void temporaire() 
	{
		CurrentPlayer.lifePoints = 100;
		CurrentPlayer.playerName = "Guillaume";
		CurrentPlayer.playerID = 213;
		TowerList.towerAttack = new Tower("ARD", 104, 0,0, false, new Player("Slut"));
		Server server = new Server(this);
		String idSaka = server.send(13, "220", "ARD");
		popUp(idSaka);
		idTour = server.send(14, "220", idSaka);
		popUp(idTour);
	}

	private void draw()
	{
		list = new ArrayList<View>();
		char blocc ;
		View bloc;
		for(int i = 0 ; i < tower.length() ; i++)
		{
			blocc = tower.charAt(i);
			switch(blocc)
			{
			case 'A':
				bloc = new View(this);
				bloc.setBackgroundResource(R.layout.rectangle_red);
				bloc.setLayoutParams(param);
				list.add(bloc);
				break;
			case 'D':
				bloc = new View(this);
				bloc.setBackgroundResource(R.layout.rectangle_blue);
				bloc.setLayoutParams(param);
				list.add(bloc);
				break;
			case 'R':
				bloc = new View(this);
				bloc.setBackgroundResource(R.layout.rectangle_green);
				bloc.setLayoutParams(param);
				list.add(bloc);
				break;
			}
		}
		blocs.removeAllViews();
		for(int i = list.size() -1 ; i >=0 ; i-- )
			blocs.addView(list.get(i));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.attack, menu);
		return true;
	}

	public void updateLifeBar()
	{
		int life = CurrentPlayer.lifePoints;
		int maxLife = CurrentPlayer.getMaxLife();
		health.setText("Points de vie :"+life+"/"+maxLife);
		mProgress.setProgress(100*life/maxLife);

	}
	private void playerIsDead() 
	{
		write("La tour t'a vaincue !", Color.RED);
		returnButton.setBackgroundColor(0xff568203);
		attackButton.setVisibility(View.INVISIBLE);
	}
	
	private void towerIsDead()
	{
		write("Tu as remporté l'affrontement :D", Color.RED);
		returnButton.setBackgroundColor(0xff568203);
		attackButton.setVisibility(View.INVISIBLE);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.Aattack:
			startCountDown();
			playerAttacksTower();
			break;

		case R.id.Areturn:
			finish();
			break;
		}
	}

	private void playerAttacksTower()
	{
		int error;
		String[] results = null;
		Server server = new Server(this);
		String result = server.send(17, Integer.toString(CurrentPlayer.playerID), idTour);
		try
		{
			results= result.split("/");
			if(results.length != 2)
			{
				error = -4;
			}
			else
			{
				xp =Integer.parseInt(results[1]);
				error = Integer.parseInt(results[0]);
				if(error == 1)
					throw new NumberFormatException();
			}
			switch(error)
			{
				case -1:
					write("Problème interne au serveur", 0xff568203);
					break;
				case -2: 
					write("Tu ne peux pas attaquer ta propre tour !", 0xff568203);
					break;
				case -3:
					write("Le joueur est trop loin de sa tour", 0xff568203);
					break;
				default:
					write("Erreur serveur :"+result, 0xff568203);
					break;
			}
		}
		catch(NumberFormatException e)
		{
			//result = result.split("br />")[2];
			if(notValid(results[0]))
				write("Erreur message :"+result, 0xff568203);
			else
			{
				previousTower = tower;
				tower = results[0];
				write("Tu attaques la tour");
				writeMessageTower();
				if(tower.compareTo("1") == 0)
				{
					tower = "";
					towerIsDead();
				}
				draw();
			}
		}
	}

	
	private boolean notValid(String tour)
	{
		boolean result = true;
		char c;
		for(int i = 0 ; i < tour.length() ; i++)
		{
			c = tour.charAt(i);
			if (c != 'A' && c != 'D' && c != 'R')
				result = false;
		}
		return !(tour.compareTo("1") ==0 || result) ;
	}

	private void writeMessageTower() 
	{
		int[] newTower = countBlocs(tower);
		int[] oldTower = countBlocs(previousTower);
		boolean kill = true;
		if(oldTower[1] > 0)
		{
			if (newTower[1] == oldTower[1])
			{
				write("Un bloc défense a été endommagé", Color.BLUE);
				kill = false;
			}
			else if (newTower[1] == oldTower[1] -1)
				write("Un bloc défense a été détruit", Color.BLUE);
			else write("Erreur 1", Color.RED);
		}
		else
		{
			if(newTower[0] == oldTower[0] -1)
				write("Un bloc attaque a été détruit", Color.BLUE);
			else if(newTower[2] == oldTower[2] -1)
				write("Un bloc ressource a été détruit", Color.BLUE);
			else write("Erreur 2", Color.RED);		
		}
		if(kill)
		{
			write("Tu remportes "+xp+" xp");
			CurrentPlayer.setXpCount(this, xp);
		}
	}
	
	private int[] countBlocs(String tower) // Retourne un tableau contenant le nombre de Blocs A/D/R de la tour
	{
		int[] types = new int[3];
		types[0] = 0;
		types[1] = 0;
		types[2] = 0;
		String type = "";

		for(int i = 0 ; i < tower.length() ; i++)
		{
			type = Character.toString(tower.charAt(i));
			if (type.compareTo("A") == 0)
				types[0]++;
			if (type.compareTo("D") == 0)
				types[1]++;
			if (type.compareTo("R") == 0)
				types[2]++;
		}
		return types;
	}

	private boolean tempAttack()
	{
		String reponse = null;
		Server server = new Server(this);
		int lifePoints = 0 ;
		try
		{
			reponse = server.send(18, Integer.toString(CurrentPlayer.playerID), idTour);
			lifePoints = Integer.parseInt(reponse);
			
			if (lifePoints < 0)
				throw new ErrorResponseFormatException();
		}
		catch(NumberFormatException e)
		{
			write("Erreur serveur :"+reponse, 0xff568203);
			lifePoints = 0;
			return false;
		} 
		catch (ErrorResponseFormatException e) 
		{
			switch(lifePoints)
			{
				case -1:
					write("Problème interne serveur", 0xff568203);
					break;
				case -2:
					write("La tour ne peut pas attaquer son propriétaire", 0xff568203);
					break;
				case -3:
					write("Le joueur est trop loin de la tour pour l'attaquer", 0xff568203);
					break;
				case -5:
					write("Le joueur est déjà mort", 0xff568203);
					break;
				default:
					write("Erreur serveur", 0xff568203);
					break;
			}
			return false;
		}
		oldLifePoints = CurrentPlayer.lifePoints;
		CurrentPlayer.lifePoints = lifePoints;
		CurrentPlayer.save(this);
		updateLifeBar();
		write("La tour t'attaque");
		writeMessagePlayer(lifePoints);
		if (lifePoints == 0)
			playerIsDead();
		return true;
	}
	
	private void writeMessagePlayer(int newLifePoints) 
	{
		write("Elle t'inflige "+(oldLifePoints - newLifePoints)+" points de vie", Color.BLUE);
	}

	/*private void towerAttacksPlayer()
	{
		TowerAttackPlayerTimer tapt = new TowerAttackPlayerTimer(this,CurrentPlayer.playerID, idTour );
		timer.schedule(tapt, 0, 10000);
		
		if(CurrentPlayer.lifePoints >0)
		{
			CurrentPlayer.lifePoints -= 10;
			if (CurrentPlayer.lifePoints < 0)
				CurrentPlayer.lifePoints = 0;
			updateLifeBar();
		}
	}*/

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	private void startCountDown()
	{
		attackButton.setClickable(false);
		new CountDownTimer(timeAttack*1000, 1000) 
		{
			private boolean attackHasBeenLaunched = false;

			public void onTick(long millisUntilFinished) 
			{
				attackButton.setText(""+millisUntilFinished / 1000);
				if(!attackHasBeenLaunched && millisUntilFinished / 1000 == 3 && tower.length() != 0)
				{
					attackHasBeenLaunched = true;
					tempAttack();
				}
					
			}

			public void onFinish() 
			{
				attackButton.setText("Attaquer");
				attackButton.setClickable(true);
			}
		}.start();
	}
	
	private void write(String message, int color)
	{
		TextView t = new TextView(this);
		LinearLayout.LayoutParams parametres;
		parametres = new LinearLayout.LayoutParams (LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		t.setLayoutParams(parametres);
		t.setText(message);
		t.setTextColor(color);
		t.setTextSize(14);
		logs.addView(t);
	}
	
	private void write(String message)
	{
		write(message, Color.BLACK);
	}
	
	
	/*public class TowerAttackPlayerTimer extends TimerTask
	{
		private Context context;
		private String playerId;
		private String towerId;
		private int lifePoints;
		
		public TowerAttackPlayerTimer(Context context, int playerId, String towerId)
		{
			this.context = context;
			this.playerId = Integer.toString(playerId);
			this.towerId = towerId;
		}

		@Override
		public void run() 
		{
			String reponse = null;
			Server server = new Server(context);
			try
			{
				reponse = server.send(18, playerId, towerId);
				lifePoints = Integer.parseInt(reponse);
				
				if (lifePoints < 0)
					throw new ErrorResponseFormatException();
			}
			catch(NumberFormatException e)
			{
				popUp("Erreur serveur :"+reponse);
				lifePoints = 0;
			} catch (ErrorResponseFormatException e) 
			{
				if(lifePoints == -1)
					popUp("Problème interne serveur");
				if(lifePoints == -2)
					popUp("La tour attaque son propriétaire");
				if(lifePoints == -3)
					popUp("Le joueur est trop loin de la tour pour l'attaquer");
				if(lifePoints == -4)
					popUp("Erreur serveur");
				if(lifePoints == -5)
					popUp("Le joueur est déjà mort");
			}
		}
		
		private void popUp(String message)
		{
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		}
		
		private void main()
		{
			Player player = CurrentPlayer.getPlayer();
			//player.addHealthListener(new HealthAdapter(){
			//		
			//	});
		}
	}



	@Override
	public void onHealthChanged() 
	{
		
	}*/
}
