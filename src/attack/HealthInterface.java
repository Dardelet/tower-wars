package attack;

import java.util.EventListener;

public interface HealthInterface extends EventListener
{
	void onHealthChanged();
}
