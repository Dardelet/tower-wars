package headQuartersActivities;

import java.util.ArrayList;

import main.MenuActivity;

import com.pact.towerwars.R;

import sousMenuActivites.MapActivity;
import structures.CurrentPlayer;
import structures.Players;
import structures.Tower;
import structures.TowerList;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DisplayTowerActivity
extends Activity 
implements View.OnClickListener 

{
	private ArrayList<Tower> towers;
	private int numeroTour = 0; // Représente le numéro de la tour à afficher dans la liste. Il est calculé modulo la taille de la liste
	private int idTour;
	private String etat;
	private int lifePoints;
	private double posX;
	private double posY;
	
	private int nombreDeABloc;
	private int nombreDeDBloc;
	private int nombreDeRBloc;
	private Button buildTower;
	private ViewGroup blocs;
	private LinearLayout.LayoutParams param;
	private ArrayList<View> list = new ArrayList<View>();


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.display); 

		towers  = TowerList.towers;// Par l'intermédiaire d'une variable globale, on récupère la liste des tours à afficher
								   // On ne peut pas faire un intent car une arrayList n'implémente pas la classe Parceable
		blocs = (ViewGroup) findViewById(R.id.blocs);
		param = new LinearLayout.LayoutParams (150, 150);
		param.gravity = Gravity.CENTER;
		Button returnHQ = (Button) findViewById(R.id.returnHQ);
		Button nextTower = (Button) findViewById(R.id.next_tower);
		Button previousTower = (Button) findViewById(R.id.previous_tower);
		buildTower = (Button) findViewById(R.id.build_tower);
		loadTower();
		returnHQ.setOnClickListener(this);
		nextTower.setOnClickListener(this);
		previousTower.setOnClickListener(this);
		buildTower.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_tower, menu);
		return true;
	}

	public int[] countBlocs(Tower tower) // Retourne un tableau contenant le nombre de Blocs A/D/R de la tour
	{
		String blocs = tower.getTower();
		int[] types = new int[3];
		types[0] = 0;
		types[1] = 0;
		types[2] = 0;
		String type = "";

		for(int i = 0 ; i < blocs.length() ; i++)
		{
			type = Character.toString(blocs.charAt(i));
			if (type.compareTo("A") == 0)
				types[0]++;
			if (type.compareTo("D") == 0)
				types[1]++;
			if (type.compareTo("R") == 0)
				types[2]++;
		}
		return types;

	}

	public void loadTower()
	{
		list = new ArrayList<View>();
		numeroTour = numeroTour % towers.size();
		if (numeroTour < 0) 
			numeroTour += towers.size();
		Tower tour = towers.get(numeroTour);
		idTour =  tour.getId();
		
		if (tour.getIsPosed())
			buildTower.setVisibility(View.INVISIBLE);
		else
			buildTower.setVisibility(View.VISIBLE);
	/*
	buildTower.setText("Voir");
		etat = tour.getState();
		if (etat.compareTo("Non construite") == 0)
			buildTower.setText("Poser");
		proprietaire = tour.getPlayer().getPlayerName();
		posX = tour.getPosX();
		posY = tour.getPosY();
		int[] nombreDeBloc = countBlocs(tour);
		nombreDeABloc = nombreDeBloc[0];
		nombreDeDBloc = nombreDeBloc[1];
		nombreDeRBloc = nombreDeBloc[2];
		lifePoints = nombreDeDBloc * CurrentPlayer.lvlDBloc;
		*/
		TextView num = (TextView) findViewById(R.id.num_tower);
		/*TextView state = (TextView) findViewById(R.id.state_tower);
		TextView owner = (TextView) layout.findViewById(R.id.owner_tower);
		TextView lifePoints = (TextView) findViewById(R.id.lifepoints_tower);
		TextView posX = (TextView) findViewById(R.id.posx_tower);
		TextView posY = (TextView) findViewById(R.id.posy_tower);
		TextView ABloc = (TextView) layout.findViewById(R.id.bloca_tower);
		TextView DBloc = (TextView) layout.findViewById(R.id.blocd_tower);
		TextView RBloc = (TextView) layout.findViewById(R.id.blocr_tower);*/
		num.setText(Integer.toString(numeroTour));
		/*state.setText(etat);
		
		//owner.setText(proprietaire);
		lifePoints.setText(Integer.toString(this.lifePoints));
		posX.setText("x = "+Double.toString(this.posX));
		posY.setText("y = "+Double.toString(this.posY));
		ABloc.setText(Integer.toString(nombreDeABloc));
		DBloc.setText(Integer.toString(nombreDeDBloc));
		RBloc.setText(Integer.toString(nombreDeRBloc));*/
		
		draw();
	}

	private void draw() 
	{
		String tour = towers.get(numeroTour).getTower();
		char blocc ;
		View bloc;
		for(int i = 0 ; i < tour.length() ; i++)
		{
			blocc = tour.charAt(i);
			switch(blocc)
			{
				case 'A':
					bloc = new View(this);
					bloc.setBackgroundResource(R.layout.rectangle_red);
					bloc.setLayoutParams(param);
					list.add(bloc);
					break;
				case 'D':
					bloc = new View(this);
					bloc.setBackgroundResource(R.layout.rectangle_blue);
					bloc.setLayoutParams(param);
					list.add(bloc);
					break;
				case 'R':
					bloc = new View(this);
					bloc.setBackgroundResource(R.layout.rectangle_green);
					bloc.setLayoutParams(param);
					list.add(bloc);
					break;
			}
		}
		blocs.removeAllViews();
		for(int i = list.size() -1 ; i >=0 ; i-- )
			blocs.addView(list.get(i));
	}
	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) // Sur quel bouton avons nous cliqué ?

		{ // Pour chaque bouton, on lance l'activité correspondante
		case R.id.returnHQ:
			Intent ii = new Intent(); 
			ii.putExtra("idTour", 0); 
			setResult(RESULT_OK, ii);

			finish();
			break;

		case R.id.next_tower:
			numeroTour++;
			loadTower();
			break;

		case R.id.previous_tower:
			numeroTour--;
			loadTower();
			break;
		case R.id.build_tower:
			Intent i = getIntent();
			if ( i.getIntExtra("Lancé par MapActivity", 0)== 1)
			{
				Intent result = new Intent(); 
				result.putExtra("idTour", idTour); 
				setResult(RESULT_OK, result);
				finish();
			} 
			else 
			{
				popUp("Appuie longtemps sur la carte pour la poser");
				Intent mapIntent = new Intent(this, MapActivity.class);
				mapIntent.putExtra("TourAPoser", idTour);
				startActivityForResult(mapIntent, 42);
			}
			break;
		}			
	}
	
	private void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{	
		if (requestCode == 42) 
		{
			finish();
		} 
	}
	
	
}
