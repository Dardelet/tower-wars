package headQuartersActivities;

import com.pact.towerwars.R;

import serveur.NoResponseException;
import serveur.ServerThread;
import structures.Activity2;
import structures.CurrentPlayer;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;


public class ManageRessourcesActivity  // L'état des tours est actualisé seulement au début
	extends Activity  // Consulte l'état du personnage
	implements View.OnClickListener
{
	private double goldAmountTower;
	private TextView goldTower;
	private TextView goldPlayer;
	private Button returnHQ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		LinearLayout layout = (LinearLayout) LinearLayout.inflate(this, R.layout.activity_manage_ressources, null);
		
		goldPlayer = (TextView) layout.findViewById(R.id.gold_player);
		goldTower = (TextView) layout.findViewById(R.id.gold_tower);
		TextView levelPlayer = (TextView) layout.findViewById(R.id.level_player);
		TextView xpActuelle = (TextView) layout.findViewById(R.id.xp_actuelle);
		TextView xpMax = (TextView) layout.findViewById(R.id.xp_max);
		returnHQ = (Button) layout.findViewById(R.id.returnHQ_manage_ressource);
				
		setContentView(layout);
		getGold(); // On actualise l'or du joueur en demandant au server
		
		goldPlayer.setText(Long.toString(CurrentPlayer.goldAmount));
		goldTower.setText(Double.toString(goldAmountTower));
		levelPlayer.setText(Integer.toString(CurrentPlayer.level));
		xpActuelle.setText(Integer.toString(CurrentPlayer.xpCount));
		xpMax.setText(Integer.toString(CurrentPlayer.xpCountMax()));
		
		returnHQ.setOnClickListener(this);
	}
	
	protected void onResume() 
	{
		CurrentPlayer.load(this);
		super.onResume();
	}

	protected void onPause() 
	{
		CurrentPlayer.save(this);
		super.onPause();
	}

	private void getGold() 
	{
		ServerThread server = new ServerThread(this);
		server.send(6, Integer.toString(CurrentPlayer.playerID), // L'instruction 6 est "mettreAJourJoueur"
				Integer.toString(CurrentPlayer.posX),
				Integer.toString(CurrentPlayer.posY),
				Integer.toString(CurrentPlayer.level),
				Integer.toString(CurrentPlayer.xpCount),
				"0",
				"0",
				"0");
		
		server.takeANap();
		String donnee = ServerThread.reponse;
		if (donnee.compareTo("-4") == 0)
		{
			popUp("Pas de réseau :/");
			finish();
		}
		else
		{
			int index = donnee.indexOf("/");
			try
			{
				CurrentPlayer.goldAmount = (int) Double.parseDouble(donnee.substring(0, index));
				goldAmountTower = Double.parseDouble(donnee.substring(index+1));
			}
			catch(NumberFormatException e)
			{
				popUp("Erreur format :"+donnee);
			}
		}
		
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_ressources, menu);
		return true;
	}
	
	

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
{
		case R.id.returnHQ_manage_ressource:
			finish();
			break;
		}
	}
	
	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}


}
