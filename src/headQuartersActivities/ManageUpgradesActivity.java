package headQuartersActivities;

import serveur.ServerThread;
import structures.Activity2;
import structures.CurrentPlayer;

import com.pact.towerwars.R;

import android.view.View;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ManageUpgradesActivity 
extends Activity
implements View.OnClickListener
{
	private TextView lvlABloc;
	private TextView lvlDBloc;
	private TextView lvlRBloc;
	private TextView costABloc;
	private TextView costDBloc;
	private TextView costRBloc;
	private TextView goldPlayer;
	private int prixABloc;
	private int prixDBloc;
	private int prixRBloc;
	private int upgradedABloc;
	private int upgradedDBloc;
	private int upgradedRBloc;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		LinearLayout layout = (LinearLayout) LinearLayout.inflate(this, R.layout.activity_manage_upgrades, null);

		goldPlayer = (TextView) layout.findViewById(R.id.gold_player_upgrades);
		lvlABloc = (TextView) layout.findViewById(R.id.lvl_a_bloc);
		lvlDBloc = (TextView) layout.findViewById(R.id.lvl_d_bloc);
		lvlRBloc = (TextView) layout.findViewById(R.id.lvl_r_bloc);
		costABloc = (TextView) layout.findViewById(R.id.cost_a_bloc);
		costDBloc = (TextView) layout.findViewById(R.id.cost_d_bloc);
		costRBloc = (TextView) layout.findViewById(R.id.cost_r_bloc);
		Button upgradeABloc = (Button) layout.findViewById(R.id.upgrade_a_bloc);
		Button upgradeDBloc = (Button) layout.findViewById(R.id.upgrade_d_bloc);
		Button upgradeRBloc = (Button) layout.findViewById(R.id.upgrade_r_bloc);
		Button returnHQ = (Button) layout.findViewById(R.id.returnHQ_upgrade);

		lvlABloc.setText(Integer.toString(CurrentPlayer.lvlABloc));
		lvlDBloc.setText(Integer.toString(CurrentPlayer.lvlDBloc));
		lvlRBloc.setText(Integer.toString(CurrentPlayer.lvlRBloc));
	
		calculateBlocPrice(); // Calcule et affiche le prix des blocs
			
		
		setContentView(layout);
		
		getGold(); // demande au serveur d'actualiser l'argent du joueur avec l'argent gagné par les tours
		goldPlayer.setText(Long.toString(CurrentPlayer.goldAmount));
		CurrentPlayer.save(this);
		
		upgradeABloc.setOnClickListener(this);
		upgradeDBloc.setOnClickListener(this);
		upgradeRBloc.setOnClickListener(this);
		returnHQ.setOnClickListener(this);

	}
	
	

	public void calculateBlocPrice()
	{
		prixABloc = 100*(1 + CurrentPlayer.lvlABloc);
		prixDBloc = 100*(1 + CurrentPlayer.lvlDBloc);
		prixRBloc = 100*(1 + CurrentPlayer.lvlRBloc);
		costABloc.setText(Integer.toString(prixABloc));
		costDBloc.setText(Integer.toString(prixDBloc));
		costRBloc.setText(Integer.toString(prixRBloc));
	}
	
	private void getGold() 
	{
		ServerThread server = new ServerThread(this);
		server.send(6, Integer.toString(CurrentPlayer.playerID),
				Integer.toString(CurrentPlayer.posX),
				Integer.toString(CurrentPlayer.posY),
				Integer.toString(CurrentPlayer.level),
				Integer.toString(CurrentPlayer.xpCount),
				"0",
				"0",
				"0");
		server.takeANap();
		
		String donnee = ServerThread.reponse;
		if (donnee.compareTo("-4") == 0)
		{
			popUp("Pas de réseau :/");
			finish();
		}
		else
		{
			int index = donnee.indexOf("/");
			try
			{
				CurrentPlayer.goldAmount = (int) Double.parseDouble(donnee.substring(0, index));
			}
			catch(NumberFormatException e)
			{
				popUp("Erreur format :"+donnee);
			}
		}
	}
	
	protected void onResume() 
	{
		
		CurrentPlayer.load(this);
		super.onResume();
	}

	protected void onPause() 
	{
		CurrentPlayer.save(this);
		super.onPause();
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_upgrades, menu);
		return true;
	}

	@Override
	public void onClick(View v) 
	{
		upgradedABloc = 0;
		upgradedDBloc = 0;
		upgradedRBloc = 0;
		switch(v.getId())
		{
		case R.id.upgrade_a_bloc:
			upgradedABloc = 1; // On indique quel bloc vient d'être amélioré
			if (prixABloc <= CurrentPlayer.goldAmount && deducePriceFromPlayerAccount())
			{				
				CurrentPlayer.lvlABloc += 1 ; 
				CurrentPlayer.goldAmount -= prixABloc;
				CurrentPlayer.setXpCount(this, xpBlocUpdate());

				lvlABloc.setText(Integer.toString(CurrentPlayer.lvlABloc));
				goldPlayer.setText(Long.toString(CurrentPlayer.goldAmount));

			}
			else 
				popUp("Pas assez d'or");
			break;

		case R.id.upgrade_d_bloc:
			upgradedDBloc = 1;
			if (prixDBloc <= CurrentPlayer.goldAmount && deducePriceFromPlayerAccount())
			{
				CurrentPlayer.lvlDBloc += 1 ; 
				CurrentPlayer.goldAmount -= prixABloc;
				CurrentPlayer.setXpCount(this, xpBlocUpdate());
				lvlDBloc.setText(Integer.toString(CurrentPlayer.lvlDBloc));
				goldPlayer.setText(Long.toString(CurrentPlayer.goldAmount));
			}
			else 
				popUp("Pas assez d'or");
			break;

		case R.id.upgrade_r_bloc:
			upgradedRBloc = 1;
			if (prixRBloc <= CurrentPlayer.goldAmount && deducePriceFromPlayerAccount())
			{		
				CurrentPlayer.lvlRBloc += 1 ; 
				CurrentPlayer.goldAmount -= prixRBloc;
				CurrentPlayer.setXpCount(this, xpBlocUpdate());
				lvlRBloc.setText(Integer.toString(CurrentPlayer.lvlRBloc));
				goldPlayer.setText(Long.toString(CurrentPlayer.goldAmount));
			}
			else 
				popUp("Pas assez d'or");
			break;

		case R.id.returnHQ_upgrade:
			finish();
			break;
			
		}
		calculateBlocPrice();
	}

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
	
	public boolean deducePriceFromPlayerAccount() 
	{
		// Le server calcul tout seul le prix à déduire en fonction du nombre de bloc à upgrade
		ServerThread server = new ServerThread(this);
		server.send(6, Integer.toString(CurrentPlayer.playerID),
				Double.toString(CurrentPlayer.posX),
				Double.toString(CurrentPlayer.posY),
				Integer.toString(CurrentPlayer.level),
				Integer.toString(CurrentPlayer.xpCount),
				Integer.toString(upgradedABloc),
				Integer.toString(upgradedDBloc),
				Integer.toString(upgradedRBloc));

		server.takeANap();
		String donnee = ServerThread.reponse;
		if (donnee.compareTo("-4") == 0)
		{
			popUp("Pas de réseau :/");
			return false;
		}
		else
		{		
			return ServerThread.reponse.compareTo("0") != 0;
		}
	}
	
	private int xpBlocUpdate()
	{
		return 35;
	}

}
