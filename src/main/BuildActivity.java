package main;

import java.util.ArrayList;

import com.pact.towerwars.R;

import serveur.ServerThread;
import structures.CurrentPlayer;
import structures.CurrentTowers;
import structures.Player;
import structures.Tower;
import structures.Towers;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class BuildActivity extends Activity 
implements OnClickListener
{
	private ViewGroup blocs;
	private LinearLayout.LayoutParams param;
	private Button ABloc;
	private Button DBloc;
	private Button RBloc;
	private Button returnMenu;
	private Button save;
	private Button delete;
	private TextView goldPlayer;
	private TextView maxHeight;
	private TextView towerPrice;
	private int price = 0;
	private ArrayList<View> list = new ArrayList<View>();
	private String tower = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_build); 
		blocs = (ViewGroup) findViewById(R.id.blocs);
		param = new LinearLayout.LayoutParams (150, 150);
		param.gravity = Gravity.CENTER;
		goldPlayer = (TextView) findViewById(R.id.current_gold_player);
		maxHeight = (TextView) findViewById(R.id.max_bloc);
		towerPrice= (TextView) findViewById(R.id.tower_cost);
		getGold();
		goldPlayer.setText("Tu as \n "+CurrentPlayer.goldAmount+" or");
		maxHeight.setText("Max \n "+CurrentPlayer.calculateMaxHeight()+" blocs");
		towerPrice.setText("Cout\n 0 or");
		ABloc = (Button) findViewById(R.id.button1);
		DBloc = (Button) findViewById(R.id.button2);
		RBloc = (Button) findViewById(R.id.button3);
		returnMenu = (Button) findViewById(R.id.return4);
		save = (Button) findViewById(R.id.save_tower);
		delete = (Button) findViewById(R.id.delete_bloc);


		ABloc.setOnClickListener(this);
		DBloc.setOnClickListener(this);
		RBloc.setOnClickListener(this);
		returnMenu.setOnClickListener(this);
		save.setOnClickListener(this);
		delete.setOnClickListener(this);



		// Nombre max de bloc :
		// ValideR/ Enregistrer dans SAKATOUR
		// Supprimer dernier bloc
		// Visuliser Argent
		// Afficher les prix des blocs
		// Donner la possibilité de visualiser, sans modifier
		// Ajouter lien serveur
		// Ajouter lien CurrentPlayer
		// tester manage upgrade
		// Tester la dédution du prix tour à la pose

	}

	private void draw() 
	{
		blocs.removeAllViews();
		for(int i = list.size() -1 ; i >=0 ; i-- )
			blocs.addView(list.get(i));
	}

	private void getGold() 
	{
		ServerThread server = new ServerThread(this);
		server.send(6, Integer.toString(CurrentPlayer.playerID), // L'instruction 6 est "mettreAJourJoueur"
				Integer.toString(CurrentPlayer.posX),
				Integer.toString(CurrentPlayer.posY),
				Integer.toString(CurrentPlayer.level),
				Integer.toString(CurrentPlayer.xpCount),
				"0",
				"0",
				"0");

		server.takeANap();
		String donnee = ServerThread.reponse;
		if (donnee.compareTo("-4") == 0)
		{
			popUp("Pas de réseau :/");
			finish();
		}
		else
		{
			int index = donnee.indexOf("/");
			CurrentPlayer.goldAmount = (int) Double.parseDouble(donnee.substring(0, index));
		}

	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.build, menu);
		return true;
	}

	@Override
	public void onClick(View v) 
	{
		View bloc;
		String bloc2 = "";
		switch(v.getId())
		{
		case R.id.button1:
			if(list.size() < CurrentPlayer.calculateMaxHeight())
			{	bloc2 = "A";
			price +=20;
			bloc = new View(this);
			bloc.setBackgroundResource(R.layout.rectangle_red);
			bloc.setLayoutParams(param);
			list.add(bloc);
			}
			else
				popUp("Tu as atteint la hauteur maximale pour ton niveau");
			break;
		case R.id.button2:
			if(list.size() < CurrentPlayer.calculateMaxHeight())
			{	bloc2 = "D";
			price +=20;
			bloc = new View(this);
			bloc.setBackgroundResource(R.layout.rectangle_blue);
			bloc.setLayoutParams(param);
			list.add(bloc);
			}
			else
				popUp("Tu as atteint la hauteur maximale pour ton niveau");
			break;
		case R.id.button3:
			if(list.size() < CurrentPlayer.calculateMaxHeight())
			{	bloc2 = "R";
			price +=20;
			bloc = new View(this);
			bloc.setBackgroundResource(R.layout.rectangle_green);
			bloc.setLayoutParams(param);
			list.add(bloc);
			}
			else
				popUp("Tu as atteint la hauteur maximale pour ton niveau");
			break;
		case R.id.return4:
			popUp("La tour n'a pas été sauvegardée :(");
			finish();
			break;
		case R.id.save_tower:
			if(price == 0)
				popUp("Il te faut au moins un bloc !");	
			else
				saveTower();	
			break;
		case R.id.delete_bloc:
			if(tower.length() > 0)
			{
				list.remove(list.size() -1);
				tower = tower.substring(0, tower.length()-1);
				price -= 20;
			}
			else
				popUp("Aucun bloc à supprimer !");
			break;

		default:
			break;

		}
		tower +=bloc2;
		towerPrice.setText("Cout\n "+price+" or");
		draw();
	}

	private void saveTower() 
	{
		ServerThread server = new ServerThread(this);
		server.send(13, Integer.toString(CurrentPlayer.playerID), tower);
		String reponse = server.takeANap();
		int id;
		if (reponse == null)
			id = -4;
		else
		{
			try
			{
				id = Integer.parseInt(reponse);
			}
			catch (NumberFormatException e)
			{
				id = 0;
			}
		}
		if (id <= 0)
		{
			if(id == 0)
				popUp("Erreur Serveur");
			if(id == -1)
				popUp("Tu n'as pas assez d'argent");
			if(id == -2)
				popUp("Ton niveau est trop faible pour cette hauteur");
			if(id == -4)
				popUp("Erreur Réception");
		}
		popUp("Tour enregistrée. Pour la consulter, va dans le QG");
		finish();
	}

	protected void onPause()
	{
		super.onPause();
		CurrentPlayer.save(this);
	}

	protected void onResume()
	{
		super.onResume();
		CurrentPlayer.load(this);
	}

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
