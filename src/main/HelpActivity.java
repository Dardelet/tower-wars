package main;

import java.io.InputStream;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
		WebView web = (WebView) findViewById(R.id.web_view);
		web.setWebViewClient(new WebViewClient());
        //web.loadUrl("file:///storage/emulated/0/Android/data/com.example.towerwars/AideTowerWars-1.html");
        //web.loadUrl("data/AideTowerWars.html");
        String fileName = "AideTowerWars.html";
        web.loadUrl("file:///android_asset/" + fileName);
    }
	
	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}


	

}
