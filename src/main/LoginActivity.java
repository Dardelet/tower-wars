package main;

import java.text.Normalizer;

import com.pact.towerwars.R;

import serveur.GPS;
import serveur.ServerThread;
import structures.CurrentPlayer;
import structures.Player;
import structures.Players;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user
 */
public class LoginActivity 
extends Activity 
implements View.OnClickListener 
{
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for playerName and password at the time of the login attempt.
	private String mPlayerName;
	private String mPassword;
	private Player player;
	private String message;

	// UI references.
	private EditText mPlayerNameView;
	private EditText mPasswordView;
	private View mLoginFormView;
	private View mLoginStatusView;
	private View mSignInButton;
	private Button newAccount;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_login);
		// Set up the login form.

		mPlayerNameView = (EditText) findViewById(R.id.playerName);
		if (foundAPlayer())
			mPlayerNameView.setText(mPlayerName);
		mPasswordView = (EditText) findViewById(R.id.password);
		Intent i = getIntent();
		
		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		mSignInButton = findViewById(R.id.sign_in_button);
		newAccount = (Button) findViewById(R.id.link_to_new_player);

		mSignInButton.setOnClickListener(this);
		newAccount.setOnClickListener(this);
		
		if(i.hasExtra("Password"))
		{
			String hint = (i.getStringExtra("Password"));
			mPasswordView.setText(hint);
			attemptLogin();
		}
		
		//mPasswordView.setText("alfred");
		//attemptLogin();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid playerName, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {

		// Reset errors.
		mPlayerNameView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mPlayerName = normalize(mPlayerNameView.getText().toString());
		mPassword = normalize(mPasswordView.getText().toString());

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 6) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid playerName address.
		if (TextUtils.isEmpty(mPlayerName)) {
			mPlayerNameView.setError(getString(R.string.error_field_required));
			focusView = mPlayerNameView;
			cancel = true;
		} else if ( (mPlayerName.length() > 20) || (mPlayerName.length() < 2)) {
			mPlayerNameView.setError(getString(R.string.error_invalid_playerName));
			focusView = mPlayerNameView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			mLoginStatusMessageView.setText(R.string.login_progress_signing_in);
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.setContext(this);
			mAuthTask.execute((Void) null);
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
			.alpha(show ? 1 : 0)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE: View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
			.alpha(show ? 0 : 1)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE: View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		private Context context;

		public void setContext(Context context)
		{
			this.context = context;
		}

		@Override
		protected Boolean doInBackground(Void... params) {

			boolean ok = false ;

			try 
			{
				ok = getPlayerFromServer(context);
			}
			catch (InterruptedException e) {e.printStackTrace();}

			if (ok)
				initializeCurrentPlayer();
			return ok;
		}



		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if (success) 
			{
				Intent intent = new Intent(context, MenuActivity.class);
				CurrentPlayer.gps.on();
				startActivity(intent);
			} else {
				mPasswordView
				.setError(message);
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) {

		case R.id.sign_in_button:
			attemptLogin();
			break;

		case R.id.link_to_new_player:
			Intent intent = new Intent(this, NewAccountActivity.class);
			startActivity(intent);
			break;
		}
	}

	private boolean foundAPlayer() {

		Players players = new Players(this);
		mPlayerName = players.findCurrentPlayerName(); // Retourne le nom du joueur s'il y en a un, ou "" sinon
		boolean gotIt = mPlayerName.compareTo("") != 0;
		//if (gotIt)
		//	.setText("J'ai trouvé dans mes archives le nom du dernier joueur. Est ce toi ?"); 
		return  gotIt;// Si mPlayerName est différent de "", on a trouvé un joueur
	}

	private boolean getPlayerFromServer(Context context) throws InterruptedException {
		boolean ok = true;
		ServerThread server = new ServerThread(context);
		server.send(3, mPlayerName, mPassword);
		server.takeANap();
		String reponse = ServerThread.reponse;
		String[] playerArray = reponse.split("/");
		int a;
		try
		{
			a = Integer.parseInt(playerArray[0]);
		}
		catch (NumberFormatException e)
		{
			a = -1;
		}

		if(playerArray.length == 10 && a !=-1) 
		{
			player = new Player(
					mPlayerName,
					Integer.parseInt(playerArray[0]),
					Integer.parseInt(playerArray[1]),
					Integer.parseInt(playerArray[2]),
					Integer.parseInt(playerArray[3]),
					Integer.parseInt(playerArray[4]),
					Integer.parseInt(playerArray[5]),
					Long.parseLong(playerArray[6]),
					Integer.parseInt(playerArray[7]),
					Integer.parseInt(playerArray[8]),
					Integer.parseInt(playerArray[9]));
		}
		else 
			ok = false;
		if(reponse.compareTo("-4") == 0)
			message = "Pas de réseau :/";
		else 
		{
			if(reponse.compareTo("0") == 0)
				message = "Nom de joueur ou mot de passe incorrect";
			else
				message = "Erreur Serveur : "+reponse;
		}
		return ok; // return true if everything happened as it was supposed to, false if not

	}

	private void initializeCurrentPlayer() 
	{		
		CurrentPlayer.setPlayer(player);
		CurrentPlayer.gps = new GPS(this);// Initialise un profil de nouveau joueur
		CurrentPlayer.save(this); // On sauve les infos du joueur

	}

	private String normalize(String input) // Enlève les accents
	{
		return Normalizer.normalize(input, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

	}
	
	protected void onStart()
	{
		super.onStart();
		mPasswordView.setText("");
	}
	
	private void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}


}
