// Pouvoir
// Poser depuis QG
// Refonte graphique
// Refonte Qg...


package main;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class MainActivity extends Activity 
implements View.OnClickListener
{ 
	//private String playerName ;
	//private Button newGame ;
	private Button loadGame ;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
				
		// On récupère notre layout par désérialisation. La méthode inflate retourne un View
		RelativeLayout layout= (RelativeLayout) RelativeLayout.inflate(this, R.layout.activity_main, null);
		// puis on récupère TextView grâce à son identifiant
		//newGame = (Button) layout.findViewById(R.id.new_game);
		loadGame = (Button) layout.findViewById(R.id.load_game);
		loadGame.setText("Jouer");
		setContentView(layout);
		//newGame.setOnClickListener(this);
		loadGame.setOnClickListener(this);
		
		//gps = new positionGPS(this);
		
		/*Server server = new Server(this);
		String idSaka = server.send(13, "221", "AAADR");
		popUp(idSaka);
		String idTour = server.send(14, "221", idSaka);
		popUp(idTour);
		*/
		String message = "Pour consulter l'aide, cliquer sur le menu déroulant en haut à droite";
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onClick(View v) 
	{
		Intent intent ;
		switch(v.getId())
		{
		/*case R.id.new_game:
			intent = new Intent(this, NewAccountActivity.class);
			startActivity(intent); // On lance l'activité de création d'un nouveau joueur
			break;
		*/
		case R.id.load_game:
			intent = new Intent(this,LoginActivity.class);
			startActivity(intent); // Sinon, on transmet le joueur à l'activité Menu
			//popUp(""+gps.isUpdated()+gps.getLatJoueur()+gps.getLongJoueur());
			//ArrayList<Tour> t = gps.getTours();
			//popUp(t.toString());
			break;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.add(0, 1, 0, "Aide");
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case 1:
	            Intent i = new Intent(this, HelpActivity.class);
	            startActivity(i);
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

	private void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
