package main;

import com.pact.towerwars.R;

import serveur.ServerThread;
import sousMenuActivites.HeadQuarterActivity;
import sousMenuActivites.MapActivity;
import structures.CurrentPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import realiteAugmentee.CameraProjectActivity;

public class MenuActivity extends Activity 
implements View.OnClickListener 
{
	// On nomme les boutons pour répérer depuis quelle activité on renvient quand on arrive au menu

	private Button mop;
	private Button camera;
	private Button newTower;
	private Button headQuarter;
	private TextView coucou;
	private Button logout;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		// Il faut que l'on ai tout le temps un intent qui donne le player 
		RelativeLayout layout = (RelativeLayout) RelativeLayout.inflate(this, R.layout.activity_menu, null);

		mop = (Button) layout.findViewById(R.id.mop);
		camera = (Button) layout.findViewById(R.id.camera);
		newTower = (Button) layout.findViewById(R.id.new_tower);
		headQuarter = (Button) layout.findViewById(R.id.head_quarter);
		logout = (Button) layout.findViewById(R.id.logout);
		getPlayer();

		coucou = (TextView) layout.findViewById(R.id.coucou);
		coucou.setText("Salut "+CurrentPlayer.playerName + "( id n°" + CurrentPlayer.playerID +")" );

		setContentView(layout);		

		mop.setOnClickListener(this);
		camera.setOnClickListener(this);
		newTower.setOnClickListener(this);
		headQuarter.setOnClickListener(this);
		logout.setOnClickListener(this);
		coucou.setVisibility(View.INVISIBLE);
	}


	protected void onResume() 
	{
		super.onResume();
		mop.setText("");
	}

	private void getPlayer() // Initialise la classe CurrentPlayer avec la dernière instance de la classe Player sauvegardée en mémoire
	{
		CurrentPlayer.load(this);
	}


	@Override
	public void onClick(View v) 
	{
		Intent menuIntent ;
		switch(v.getId()) // Sur quel bouton avons nous cliqué ?

		{ // Pour chaque bouton, on lance l'activité correspondante
		case R.id.mop:
			menuIntent = new Intent(this, MapActivity.class);
			mop.setText("Chargement de la carte...");
			startActivity(menuIntent);
			break;

		case R.id.camera:
			menuIntent = new Intent(this, CameraProjectActivity.class);
			startActivity(menuIntent);
			break;

		case R.id.new_tower:
			menuIntent = new Intent(this, BuildActivity.class);
			startActivity(menuIntent);
			break;

		case R.id.head_quarter:
			menuIntent = new Intent(this, HeadQuarterActivity.class);
			startActivity(menuIntent);
			break;

		case R.id.logout:
			CurrentPlayer.gps.off();
			CurrentPlayer.locUpdate = false;
			ServerThread server = new ServerThread(this);
			server.send(11, Integer.toString(CurrentPlayer.playerID)); // Logout
			logout.setText("Déconnexion...");
			server.takeANap();
			Intent result = new Intent();
			setResult(RESULT_OK, result);
			finish();
			break;

		}	
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}

