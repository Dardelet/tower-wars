package main;

import java.text.Normalizer;

import com.pact.towerwars.R;

import serveur.ServerThread;
import structures.CurrentPlayer;
import structures.Player;
import structures.Players;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity which displays a login screen to the user
 */
public class NewAccountActivity 
	extends Activity 
	implements View.OnClickListener {
	
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// Values for playerName and password at the time of the login attempt.
	private String mPlayerName;
	private String mPassword;
	private String mPassword2;
	private static int LOGIN_ACTIVITY = -42;
	private int id;
	private static Context context;

	// UI references.
	private EditText mPlayerNameView;
	private EditText mPasswordView;
	private EditText mPasswordView2;
	private View mLoginFormView;
	private View mLoginStatusView;
	private View mSignInButton;
	private Button start;
	private TextView erreur;
	private TextView mLoginStatusMessageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_new_account);

		// Set up the login form.
		
		mPlayerNameView = (EditText) findViewById(R.id.playerName);
		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView2 = (EditText) findViewById(R.id.password2);
		context = getApplicationContext();
		mLoginFormView = findViewById(R.id.login_form);
		mLoginStatusView = findViewById(R.id.login_status);
		mLoginStatusMessageView = (TextView) findViewById(R.id.login_status_message);
		start = (Button) findViewById(R.id.start);
		erreur = (TextView) findViewById(R.id.erreur);
		mSignInButton = findViewById(R.id.sign_in_button);

		start.setVisibility(View.INVISIBLE);
		erreur.setVisibility(View.INVISIBLE);
		mSignInButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.activity_login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid playerName, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptSignIn() {

		// Reset errors.
		mPlayerNameView.setError(null);
		mPasswordView.setError(null);
		mPasswordView2.setError(null);

		// Store values at the time of the login attempt.
		mPlayerName = normalize(mPlayerNameView.getText().toString());
		mPassword = normalize(mPasswordView.getText().toString());
		mPassword2 = normalize(mPasswordView2.getText().toString());
		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		
		if (TextUtils.isEmpty(mPassword)) {
			mPasswordView.setError(getString(R.string.error_field_required));
			focusView = mPasswordView;
			cancel = true;
		} else if (mPassword.length() < 6) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid playerName address.
		if (TextUtils.isEmpty(mPlayerName)) {
			mPlayerNameView.setError(getString(R.string.error_field_required));
			focusView = mPlayerNameView;
			cancel = true;
		} else if ( (mPlayerName.length() > 20) || (mPlayerName.length() < 2)) {
			mPlayerNameView.setError(getString(R.string.error_invalid_playerName));
			focusView = mPlayerNameView;
			cancel = true;
		}
	
		if (mPassword2.compareTo(mPassword) != 0)
		{
			mPasswordView2.setError("Les deux mots de passes entrés ne sont pas identiques");
			focusView = mPasswordView2;
			cancel = true;
		}
		if (cancel) 
		{
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else 
		{
			mLoginStatusMessageView.setText("Création de compte");
			showProgress(true);
			mAuthTask = new UserLoginTask();
			mAuthTask.setContext(this);
			mAuthTask.execute((Void) null);
			
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginStatusView.setVisibility(View.VISIBLE);
			mLoginStatusView.animate().setDuration(shortAnimTime)
			.alpha(show ? 1 : 0)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginStatusView.setVisibility(show ? View.VISIBLE
							: View.GONE);
				}
			});

			mLoginFormView.setVisibility(View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
			.alpha(show ? 0 : 1)
			.setListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationEnd(Animator animation) {
					mLoginFormView.setVisibility(show ? View.GONE
							: View.VISIBLE);
				}
			});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mLoginStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		private Context context;

		public void setContext(Context context)
		{
			this.context = context;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			boolean ok = false ;
			ok = save(context);
			return ok;
		}



		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			showProgress(false);

			if ((success )||(id == -4))
			{
				erreur.setVisibility(View.VISIBLE);
				start.setVisibility(View.VISIBLE);
				mSignInButton.setVisibility(View.INVISIBLE);
				start.setOnClickListener(NewAccountActivity.this);
				
			} else 
			{
				if (id == -1)
					error("Erreur serveur", context);
				if (id == -3)
					error("Ce nom existe déjà", context);
				if (id == -2)
					error("Erreur longueur", context);
				if (id == -4)
					error("Pas de réseau :/", context);
			}
		}
		
		
		@Override
		protected void onCancelled() 
		{		
			boolean ok = true;
			mAuthTask = null;
			showProgress(false);
		}
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) {

		case R.id.sign_in_button:
			final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromInputMethod(mPasswordView2.getWindowToken(), 0);
			attemptSignIn();
			break;
			
		case R.id.start:
			launchMenu();
			break;
		}
	}
	
	private void launchMenu() 
	{
		Intent loadIntent = new Intent(context, LoginActivity.class);
		loadIntent.putExtra("Password", mPassword);
		startActivityForResult(loadIntent, LOGIN_ACTIVITY);
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{	
		if (requestCode == LOGIN_ACTIVITY) 
		{
			if (resultCode == RESULT_OK)  // Si on se déconnecte à partir du menu, on termine cette activité pour revenir directement à l'écran d'accueil
				finish();
		} // On affiche un message de provenance
	}


	private boolean save(Context context) 
	{
		CurrentPlayer.initialiser(mPlayerName); // Initialise un profil de nouveau joueur
		id = sendPlayerToServer(mPlayerName, mPassword);
		boolean ok = true;
		if (id < 0)
		{	
			ok = false;
		}
		else		
		{
			CurrentPlayer.playerID = id;
		}
		CurrentPlayer.save(context); // On sauve les infos du joueur
		return ok;
	}
	
	private int sendPlayerToServer(String playerName, String password)
	{
		ServerThread server = new ServerThread(this);
		server.send(1, playerName, password);// 1 Correspond à l'instruction "nouveauJoueur"
		String reponse = server.takeANap();
		if (reponse == null)
			id = -4;
		else
		{
			try
			{
				id = Integer.parseInt(reponse);
			}
			catch (NumberFormatException e)
			{
				id = -			1;
			}
		}
		return id;
	}

	private void error(String error, Context context) 
	{
		mPlayerNameView.setError(error);
		mPlayerNameView.requestFocus();
	}
	
	private String normalize(String input) // Enlève les accents
	{
		return Normalizer.normalize(input, Normalizer.Form.NFD).replaceAll("\\p{InCombiningDiacriticalMarks}+", "");

	}
		
	
	public void popUp(String message, Context context) 
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
