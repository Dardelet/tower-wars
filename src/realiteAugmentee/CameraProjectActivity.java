package realiteAugmentee;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.Toast;

import static android.util.FloatMath.cos;
import static android.util.FloatMath.sin;
import static android.util.FloatMath.sqrt;

public class CameraProjectActivity extends Activity implements SensorEventListener {
    SensorManager sensorManager;
    Sensor magnetic;
    Sensor accelerometer;
 //   Sensor orientation;
    float[] acceleromterVector=new float[3];
    float[] magneticVector=new float[3];
    float[] resultMatrix=new float[9];
    float[] values=new float[3];
    float[] rotationMatrix = new float[16];
    public static float azimuth;
    public static float pitch;
    public static float roll;
    private Timer timer = new Timer();
    private GLClearRenderer render= new GLClearRenderer();


    GLSurfaceView glView ;
    CameraView cameraView;

     positionGPS gps ;
     Joueur joueur = new Joueur();
     Tour closest;
     Infos infos = new Infos(joueur, new ArrayList<Tour>());
     
     public void refresh(){
    	 gps = new positionGPS(this);
         joueur.setLongit(gps.getLongJoueur());
         joueur.setLat(gps.getLatJoueur());
         infos = new Infos(joueur,gps.getTours());
         ArrayList <Tour> voisinage = gps.getTours();
         
         closest = infos.proche(voisinage);
    	 Boite.j = joueur;
         Boite.i = infos;
         Boite.p = gps;
         Boite.t = closest;
         // Rendu géré dans la classe GLClearRenderer
         
     }

    /** Lors de la création de l'activité */
    public void onCreate( Bundle savedInstanceState ) {

        super.onCreate(savedInstanceState);
        // Création d'une surface GL

        gps = new positionGPS(this);
        joueur.setLongit(gps.getLongJoueur());
        joueur.setLat(gps.getLatJoueur());
        infos = new Infos(joueur,gps.getTours());
        
        ArrayList<Tour> l = gps.getTours();
        
        closest = infos.proche(l);

        Boite.j = joueur;
        Boite.i = infos;
        Boite.p = gps;
        Boite.t = closest;
        glView= new GLSurfaceView( this );
        // Réglages afin de pouvoir voir cette la vision de la caméra
        glView.setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
        glView.getHolder().setFormat( PixelFormat.TRANSLUCENT );
        // Rendu géré dans la classe GLClearRenderer
        glView.setRenderer( render );

        // Création d'une vue contenant l'image issue de la caméra
        cameraView = new CameraView( this );

        timer.schedule(new RefreshTimer(), 5000, 5000);
     
    }
    
    @Override
    protected void onStop()
    {
    	super.onStop();
    	timer.cancel();
    }
    
    public class RefreshTimer extends TimerTask
    {
		@Override
		public void run() 
		{
			refresh();
			
		}
    }
    
    private void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}
    
    protected void onResume() {
        super.onResume();
        // Gérer les capteurs :
        // Instancier le gestionnaire des capteurs, le SensorManager
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        magnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener( this, magnetic, SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener( this, accelerometer, SensorManager.SENSOR_DELAY_GAME);

        setRequestedOrientation( ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE );

        requestWindowFeature( Window.FEATURE_NO_TITLE );
        // Pour se mettre en plein écran
        getWindow().setFlags( WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN );

        
        
        gps = new positionGPS(this);
        joueur.setLongit(gps.getLongJoueur());
        joueur.setLat(gps.getLatJoueur());
        infos = new Infos(joueur,gps.getTours());
        closest = infos.proche(gps.getTours());
        Boite.j = joueur;
        Boite.i = infos;
        Boite.p = gps;
        Boite.t = closest;



        // Cette vue créée devient la vue principale
        setContentView( glView );

        // Sur tout l'écran
        addContentView( cameraView, new LayoutParams( LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT ) );
    }

    protected void onPause() {
        // Unregister all of our SensorEventListeners upon exit:
        sensorManager.unregisterListener(this, magnetic);
        sensorManager.unregisterListener(this, accelerometer);
        super.onPause();

    }


    public void onSensorChanged(SensorEvent event) {
// Mettre à jour la valeur de l'accéléromètre et du champ magnétique
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
          //acceleromterVector=event.values;
         // Log.d("myTag", acceleromterVector[0]+"   "  +   acceleromterVector[1]+"   "+acceleromterVector[2]);

            // Nous allons smoother les valeurs afin d'éviter de percevoir le bruit
            acceleromterVector[0]= (acceleromterVector[0]*4+event.values[0])*0.2f;
            acceleromterVector[1]= (acceleromterVector[1]*4+event.values[1])*0.2f;
            acceleromterVector[2]= (acceleromterVector[2]*4+event.values[2])*0.2f;

        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            //magneticVector=event.values;
            //Log.d("Magentic values", magneticVector[0]+"   "  +   magneticVector[1]+"   "+magneticVector[2]);

           magneticVector[0]=(magneticVector[0]*1+event.values[0])*0.5f;
            magneticVector[1]=(magneticVector[1]*1+event.values[1])*0.5f;
            magneticVector[2]= (magneticVector[2]*1+event.values[2])*0.5f;
        }

       if ((event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD) || (event.sensor.getType()==Sensor.TYPE_ACCELEROMETER)) {
           SensorManager.getRotationMatrix(rotationMatrix, null, acceleromterVector, magneticVector);
           // Demander au SensorManager le vecteur d'orientation associé (values)
           SensorManager.getOrientation(rotationMatrix, values);
           SensorManager.remapCoordinateSystem(rotationMatrix, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, rotationMatrix );
       }

        azimuth =(float) Math.toDegrees(values[0]);

// le pitch
        pitch = (float) Math.toDegrees(values[1]);
// le roll
        roll = (float) Math.toDegrees(values[2]);

      Log.d("Mytag","azimuth "+azimuth+" pitch "+pitch+ "roll  "+ roll);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        
    }


}