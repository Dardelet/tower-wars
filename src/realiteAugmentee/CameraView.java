package realiteAugmentee;

import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

public class CameraView extends SurfaceView implements Callback {
    private Camera camera;
 
    public CameraView( Context context ) {
        super( context );
        // Implémentation du CallBack
        getHolder().addCallback( this );
        // On recoit sur la surface toutes les flux de données qui viennent de la caméra
        getHolder().setType( SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS );
    }
 
    public void surfaceCreated( SurfaceHolder holder ) {
        // Une fois la surface créée, on ouvre l'image issue de la caméra de l'appareil et on la place sur cette surface
        camera = Camera.open();
    }
 
    public void surfaceChanged( SurfaceHolder holder, int format, int width, int height ) {
        // Implémentation de la méthode surfaceChanged de CallBack, si on modifie la surface (taille,...)
        Camera.Parameters p = camera.getParameters();
        p.setPreviewSize( width, height );
      //  camera.setParameters( p );
 

        try {
            camera.setPreviewDisplay( holder );
        } catch( IOException e ) {
            e.printStackTrace();
        }
        // On débute la prévisualisation de la caméra
        camera.startPreview();
    }
 
    public void surfaceDestroyed( SurfaceHolder holder ) {
        // Lorsque la surface est détruite, on libère la caméra dont on a plus besoin
        camera.stopPreview();
        camera.release();
        camera = null;
    }
}