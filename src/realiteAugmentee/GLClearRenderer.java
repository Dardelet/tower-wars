package realiteAugmentee;

import javax.microedition.khronos.egl.EGLConfig;
import java.util.*;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLU;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import java.util.ArrayList;

import static android.util.FloatMath.cos;
import static android.util.FloatMath.sin;
import static java.lang.Math.sqrt;
import static java.lang.Math.tan;


public class GLClearRenderer implements Renderer {
	
	 private Cube mCube = new Cube();


    /*public Joueur joueur;
    static public ArrayList<Tour> Sakatour = new ArrayList<Tour>(10);
    public static final int R=6371000;
    static public Tour proche=Sakatour.get(0);
    public double dist=0.0;

    ArrayList<android.R.integer> Distance=new ArrayList<android.R.integer>(10);

    public int l=Sakatour.size();
    public double distance(Tour tour, Joueur player)
    {
        double distance=0;
        distance=sqrt( (R*R*(tour.getLat()-player.getLat())^2) + (R*R*(tour.getLongit()-player.getLongit())^2) );
        return distance;
    }

     public Tour proche(Joueur player)
    {
        int i=0;
        proche=Sakatour.get(0);
        dist=0.0;
        while(i<l)
        {
            if(distance(Sakatour.get(i),player)<dist)
            {
                proche=Sakatour.get(i);
                dist=distance(Sakatour.get(i), player);
            }
        }
        return proche;
    }

*/

    public void onDrawFrame( GL10 gl ) {

        /*Tour tour1 = null, tour2=null;
        tour1.setLat(300);
        tour1.setLongit(200);
        tour2.setLat(15);
        tour2.setLongit(32);
        Sakatour.add(0,tour1);
        Sakatour.add(1,tour2);

        joueur.setLat(280);
        joueur.setLongit(160);*/

        // This method is called per frame, as the name suggests.
        // For demonstration purposes, I simply clear the screen with a random translucent gray.
        //float c = 1.0f / 256 * ( System.currentTimeMillis() % 256 );
        //gl.glClearColor( c, c, c, 0.5f );
        //gl.glClear( GL10.GL_COLOR_BUFFER_BIT );
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);        
        gl.glLoadIdentity();
        /*GLU.gluLookAt(gl,0,-5,10,0,-5 + sin((float) Math.toRadians(CameraProjectActivity.azimuth)),10+
                cos((float) Math.toRadians(CameraProjectActivity.azimuth)),1,0,0);*/

        /*GLU.gluLookAt(gl,0,joueur.getLat(),joueur.getLongit(),0,joueur.getLat() + sin((float) Math.toRadians(CameraProjectActivity.azimuth)),joueur.getLongit()+
                cos((float) Math.toRadians(CameraProjectActivity.azimuth)),1,0,0);*/
        GLU.gluLookAt(gl,0,(float) Boite.j.getLat(),(float) Boite.j.getLongit(), -cos((float) Math.toRadians(CameraProjectActivity.roll)),(float) Boite.j.getLat() + sin((float) Math.toRadians(CameraProjectActivity.azimuth)),(float) Boite.j.getLongit()+
                cos((float) Math.toRadians(CameraProjectActivity.azimuth)),1,0,0);
        double x = Boite.j.getLat();
        double y = Boite.j.getLongit();
        @SuppressWarnings("unused")
		int a = 1;
        //GLU.gluLookAt(gl,+5,-5,-10,cos(CameraProjectActivity.x),0,0,1,0,0);
        //gl.glRotatef(CameraProjectActivity.azimuth,1f,0f,0f);
        //gl.glRotatef(CameraProjectActivity.z,0f,1f,0f);
        //gl.glRotatef(CameraProjectActivity.y,0f,0f,1f);
        //gl.glTranslatef(0f, -(cos(CameraProjectActivity.azimuth)),0f);
        //String s1 = Float.toString(a+((R00*A)+(R01*B)+(R02*C)));
        //String s2 = Float.toString(b+(R10*A) + (R11*B) + (R12*C));
        //String s3 = Float.toString(c+(R20*A) + (R21*B) + (R22*C));
        //Log.d("YourTag", s1 + "   " + s2 + "   " + s3);
        mCube.draw(gl);
           
        gl.glLoadIdentity();


    }

    public void onSurfaceChanged( GL10 gl, int width, int height ) {
        // This is called whenever the dimensions of the surface have changed.
        // We need to adapt this change for the GL viewport.
        gl.glViewport( 0, 0, width, height );
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();
        GLU.gluPerspective(gl, 30.0f, (float)width / (float)height, 0.00000000000000001f, 100.0f); // Regler le champ de vision proche et éloigné
        gl.glViewport(0, 0, width, height);


        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
    }
 
    public void onSurfaceCreated( GL10 gl, EGLConfig config ) {
    	gl.glClearColor(0.0f, 0.0f, 0.0f, 0.5f);
        gl.glClearDepthf(1.0f);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,
                  GL10.GL_NICEST);
    }
}