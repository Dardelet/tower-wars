package realiteAugmentee;

/**
 * Created by Aimen on 13/04/14.
 */
public class Joueur {
    private double longit;
    private double lat;

    public double getLongit() {
        return longit;
    }

    public double getLat() {
        return lat;
    }

    public void setLongit(double longJoueur) {
        this.longit = longJoueur;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
}
