package realiteAugmentee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.Layout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ServerThread extends Thread // Créé un nouveau Thread dans lequel va se faire les échanges avec le serveur
{
	private static final String url = "http://perso.telecom-paristech.fr/~beillevair/"; // Requete GET, on adjoint du code xml à l'url
	private String instructions;
	public static String reponse;
	private Context context;
	private View layout;
	private View mLoginFormView;
	private View mLoginStatusView;


	public ServerThread (Context context)
	{
		this.context = context;
	}
	
	public void run()
	{
		String data = ""; 
		String line;
		URL server; 
		HttpURLConnection connection;
		BufferedReader reader;
		try
		{
			String a = url+instructions;
			server = new URL(convertFormat(url+instructions));
			connection = (HttpURLConnection) server.openConnection();

			InputStream in = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));

			while ((line = reader.readLine()) != null) 
			{
				data += line;
			}
			connection.disconnect();
			reader.close();
			ServerThread.reponse = data;
		}
		catch (IOException e){e.printStackTrace();}
		//interrupt();
	}
	
	public String takeANap()
	{
		try 
		{
			waitResponse();
		}
		catch (Exception e)
		{
			//popUp("Echec connexion");
			e.printStackTrace();
		}
		return reponse;
	}

	
	public void popUp(String message) 
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public String waitResponse() // On attend que la variable static reponse soit remplie
			throws Exception
		{
			int i = 0;
			int waitingTime = 0;
			while((ServerThread.reponse.compareTo("-4") == 0) && i < waitingTime)
			{
				i++;
			}
			if (i == waitingTime) 
				throw new Exception();
			return ServerThread.reponse;
		}

	private String convertFormat(String instructions) // Convertit certain carctère comme ' ' '<' '>' 
	{
		String newInstructions = "";
		char c;
		String newC;

		for (int i = 0 ; i < instructions.length() ; i++)
		{
			c = instructions.charAt(i);
			switch(c)
			{
			case '<': newC = "%3C";
			break;
			case '>': newC = "%3E";
			break;
			case ' ': newC = "%20";
			break;
			case '"': newC = "%22";	
			break;
			default : newC = Character.toString(c);
			break;
			}
			newInstructions += newC;
		}
		return newInstructions;

	}

/*		public void send(int instruction, String... parametres) // On envoie une instruction et un nombre de paramètres.
		{
			reponse = "-4";
			String body = null;
			String type = null;
			switch(instruction)
			{
				case(1): 
					type = "nouveauJoueur";
					body = "<joueur"
						+ " name=\"" + parametres[0] + "\"" 
						+ " password=\"" + parametres[1] + "\"/>";
				break ;
	
				case(2):
					type = "nouvelleTour";
					body = "<joueur id=\"" + parametres[0] + "\"/>"
					+ "<tour blocs=\"" + parametres[1] + "\"/>";
				break;
				
				case(3): 
					type = "login";
					body = "<joueur"
					+ " name=\"" + parametres[0] + "\"" 
					+ " password=\"" + parametres[1] + "\"/>";
				break;
					
				case(4): 
					type = "obtenirInfosJoueur";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
	
				case(5): 
					type = "obtenirInfosTour";
					body = "<tour id=\"" + parametres[0] + "\"/>";
				break;
				
				case(6):
					type = "mettreAJourJoueur";
					body = "<joueur" 
					+ " id=\"" + parametres[0] + "\""
					+ " latitude=\"" + parametres[1] + "\""
					+ " longitude=\"" + parametres[2] + "\""
					+ " level=\"" + parametres[3] + "\""
					+ " xp=\"" + parametres[4] + "\""
					+ " lvlABloc=\"" + parametres[5] + "\""
					+ " lvlDBloc=\"" + parametres[6] + "\""			
					+ " lvlRBloc=\"" + parametres[7] + "\""
					+ "/>";
				break;
				
				case(7):
					type = "joueurAttaqueTour";
					body = "<joueur id=\"" + parametres[0] + "\"/>"
					+ "<tour id=\"" + parametres[1] + "\"/>";
				break;
				
				case(8):
					type = "supprimerJoueur";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
				
				case(9):
					type = "obtenirMapJoueurs";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
				
				case(10):
					type = "obtenirMapTours";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
				
				case(11): 
					type = "logout";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
					break;
				
				case(12):
					type = "mettreAJourOr";
					body = "<joueur" 
						 +	" id=\"" + parametres[0] + "\""  
						 +  " prix=\"" + parametres[1] + "\"/>"; 
					break;
				case(13):
					type = "mettreDansSakatour";
					body = "<joueur id=\"" + parametres[0] + "\"/>"
						+ "<tour blocs=\"" + parametres[1] + "\"/>";
					break;			
				case(14):
					type = "poserTour";
					body = "<joueur id=\"" + parametres[0] + "\"/>"
						+ "<tour id=\"" + parametres[1] + "\"/>";
					break;
					
				case(15):
					type = "getSakatour";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
				
				case(16):
					type = "getTowers";
					body = "<joueur id=\"" + parametres[0] + "\"/>";
				break;
			}
			instructions = "?xml=<action type=\"" + type + "\">" +
					body +  
					"</action>";;
			start();
		}*/
	
	public String send(int instruction, String... parametres)
	{
		Server server = new Server(context);
		reponse = server.send(instruction, parametres);
		return reponse;
	}
	
}
