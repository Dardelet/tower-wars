package realiteAugmentee;

/**
 * Created by Aimen on 13/04/14.
 */
public class Tour {

    private double longit;
    private double lat;

    public double getLongit() {
        return longit;
    }

    public double getLat() {
        return lat;
    }

    public void setLongit(double longit) {
        this.longit = longit;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }
    
    public String toString()
    {
    	return ""+longit+lat;
    }


}


