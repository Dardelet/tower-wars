package realiteAugmentee;

import java.util.ArrayList;

import structures.CurrentPlayer;

/*import serveur.ServerThread;
import structures.CurrentPlayer;*/
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

public class positionGPS 
{
	private double latitude = 48.827274;
	private double longitude= 2.346861;
	private boolean gps = false;
	private Context context;

	public positionGPS(Context context)
	{
		this.context = context;
		LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		Criteria critere = new Criteria();
		critere.setAccuracy(Criteria.ACCURACY_FINE);
		critere.setAltitudeRequired(false);
		critere.setBearingRequired(false);
		critere.setCostAllowed(false);
		critere.setPowerRequirement(Criteria.POWER_HIGH);
		critere.setSpeedRequired(false);	
		String provider=locationManager.getBestProvider(critere, true);
		Location location = locationManager.getLastKnownLocation(provider);
		if(location != null)
		{
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
		else
		{
			latitude = 48.827274;
			longitude = 2.346861;
		}
		LocationUpdate loc = new LocationUpdate();
		//locationManager.requestLocationUpdates(provider, 10000, 0, loc);
	}

	public class LocationUpdate implements LocationListener
	{
		public LocationUpdate()
		{

		}
		public void onLocationChanged(Location location) 
		{
			latitude = location.getLatitude();
			longitude= location.getLongitude();
			gps = true;
			popUp("ok");
		}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
		public void onProviderEnabled(String provider) {}
		public void onProviderDisabled(String provider) {}
	}
	
	public boolean isUpdated()
	{
		return gps;
	}
	
	public double getLatJoueur()
	{
		return latitude;
	}
	
	public double getLongJoueur()
	{
		return longitude;
	}
	
	private void popUp(String message) 
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
	
	public ArrayList<Tour> getTours()
	{
		ServerThread server = new ServerThread(context);
		String towers = server.send(10, Integer.toString(CurrentPlayer.playerID));

		String[] towersArray = towers.split("/");
		Tour tour;
		ArrayList<Tour> tours = new ArrayList<Tour>();
		for(int i = 0 ;  i < (towersArray.length)/7 ; i++)
		{
			tour = new Tour();
            tour.setLat(Double.parseDouble(towersArray[7*i+3])/1000000);
            tour.setLongit(Double.parseDouble(towersArray[7*i+2])/1000000);
			tours.add(tour);
		}
		return tours;
	}
	
	
}
