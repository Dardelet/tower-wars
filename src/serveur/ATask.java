package serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;

public class ATask extends AsyncTask<String, Void, String>
{

	private ProgressDialog progressDialog;
	private Context context;
	private static final String url = "http://perso.telecom-paristech.fr/~beillevair/"; // Requete GET, on adjoint du code xml à l'url
	private int instruction;
	private String[] parametres;
	private String xml;
	
	public ATask(Context context, int instruction, String... parametres)
	{
		this.context = context;
		this.instruction = instruction;
		this.parametres = parametres;
	}
	
	public class Clock extends TimerTask
	{
		public Clock()
		{
			
		}
		@Override
		public void run()
		{
			onPostExecute("");
		}
	}
	
	@Override
	protected String doInBackground(String... params) 
	{
		//Timer timer = new Timer();
		//timer.schedule(new Clock(), 5000);
		send(instruction, parametres);
		String data = ""; 
		String line;
		URL server; 
		HttpURLConnection connection;
		BufferedReader reader;
		try
		{
			server = new URL(convertFormat(url+xml));
			connection = (HttpURLConnection) server.openConnection();
			InputStream in = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));

			while ((line = reader.readLine()) != null) 
			{
				data += line;
			}
			connection.disconnect();
			reader.close();
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		catch (IOException e){e.printStackTrace();}
		//interrupt();
		return data;
	}
	
	@Override
    protected void onPreExecute(){
        super.onPreExecute();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            progressDialog = new ProgressDialog(context);
        } else {
            progressDialog = new ProgressDialog(context,
                    AlertDialog.THEME_HOLO_LIGHT);
        }
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Patientez");
        progressDialog.show();
    }

    @Override
    protected void onPostExecute(String result){
        super.onPostExecute(result);
        progressDialog.dismiss();
        ServerThread.reponse = result;
    
    }
    
    @Override
    protected void onCancelled()
    {
    	ServerThread.reponse = "-4";
    }
    
    
	public void send(int instruction, String... parametres) // On envoie une instruction et un nombre de paramètres.
	{
		String body = null;
		String type = null;
		switch(instruction)
		{
		case(1): 
			type = "nouveauJoueur";
		body = "<joueur"
				+ " name=\"" + parametres[0] + "\"" 
				+ " password=\"" + parametres[1] + "\"/>";
		break ;

		case(2):
			type = "nouvelleTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour blocs=\"" + parametres[1] + "\"/>";
		break;

		case(3): 
			type = "login";
		body = "<joueur"
				+ " name=\"" + parametres[0] + "\"" 
				+ " password=\"" + parametres[1] + "\"/>";
		break;

		case(4): 
			type = "obtenirInfosJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(5): 
			type = "obtenirInfosTour";
		body = "<tour id=\"" + parametres[0] + "\"/>";
		break;

		case(6):
			type = "mettreAJourJoueur";
		body = "<joueur" 
				+ " id=\"" + parametres[0] + "\""
				+ " latitude=\"" + parametres[1] + "\""
				+ " longitude=\"" + parametres[2] + "\""
				+ " level=\"" + parametres[3] + "\""
				+ " xp=\"" + parametres[4] + "\""
				+ " lvlABloc=\"" + parametres[5] + "\""
				+ " lvlDBloc=\"" + parametres[6] + "\""			
				+ " lvlRBloc=\"" + parametres[7] + "\""
				+ "/>";
		break;

		case(7):
			type = "joueurAttaqueTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(8):
			type = "supprimerJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(9):
			type = "obtenirMapJoueurs";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(10):
			type = "obtenirMapTours";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(11): 
			type = "logout";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(12):
			type = "mettreAJourOr";
		body = "<joueur" 
				+	" id=\"" + parametres[0] + "\""  
				+  " prix=\"" + parametres[1] + "\"/>"; 
		break;
		case(13):
			type = "mettreDansSakatour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour blocs=\"" + parametres[1] + "\"/>";
		break;			
		case(14):
			type = "poserTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(15):
			type = "getSakatour";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(16):
			type = "getTowers";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(17):
			type="joueurAttaqueTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(18):
			type= "tourAttaqueJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;	
		
		case(19):
			type="supprimerTour";
			body = "<tour id=\"" + parametres[0] + "\"/>";
		}
		
		xml = "?xml=<action type=\"" + type + "\">" +
				body +  
				"</action>";;
	}

	private String convertFormat(String instructions) // Convertit certain carctère comme ' ' '<' '>' 
	{
		String newInstructions = "";
		char c;
		String newC;

		for (int i = 0 ; i < instructions.length() ; i++)
		{
			c = instructions.charAt(i);
			switch(c)
			{
			case '<': newC = "%3C";
			break;
			case '>': newC = "%3E";
			break;
			case ' ': newC = "%20";
			break;
			case '"': newC = "%22";	
			break;
			default : newC = Character.toString(c);
			break;
			}
			newInstructions += newC;
		}
		return newInstructions;

	}

}
