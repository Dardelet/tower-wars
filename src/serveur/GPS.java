package serveur;

import structures.CurrentPlayer;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

public class GPS 
{
	private Context context;
	private LocationUpdate loc ;
	private LocationManager locationManager;
	private String provider;
	private Button actu = null;
	
	public GPS(Context context)
	{
		this.context = context;
		locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		Criteria critere = new Criteria();
		critere.setAccuracy(Criteria.ACCURACY_FINE);
		critere.setAltitudeRequired(false);
		critere.setBearingRequired(false);
		critere.setCostAllowed(false);
		critere.setPowerRequirement(Criteria.POWER_LOW);
		critere.setSpeedRequired(false);	
		provider = locationManager.getBestProvider(critere, true);
	}
	
	public void setButton(Button actu)
	{
		this.actu = actu;
	}
	
	public void on()
	{
		loc = new LocationUpdate();
		locationManager.requestLocationUpdates(provider, 5000, 0, loc);
	}
	
	public void off()
	{
		locationManager.removeUpdates(loc);
	}

	public class LocationUpdate implements LocationListener
	{
	    public void onLocationChanged(Location location) 
		{
			CurrentPlayer.locUpdate = true;
			if(actu != null)
				actu.setBackgroundColor(0xAA00FF00);
			CurrentPlayer.posX = (int) ((int) 1000000*location.getLatitude());
			CurrentPlayer.posY = (int) ((int) 1000000*location.getLongitude());
			ServerThread server = new ServerThread(context);
			server.send(6, Integer.toString(CurrentPlayer.playerID), // L'instruction 6 est "mettreAJourJoueur"
					Integer.toString(CurrentPlayer.posX),
					Integer.toString(CurrentPlayer.posY),
					Integer.toString(CurrentPlayer.level),
					Integer.toString(CurrentPlayer.xpCount),
					"0",
					"0",
					"0");
		}
		public void onStatusChanged(String provider, int status, Bundle extras) {}
		public void onProviderEnabled(String provider) {}
		public void onProviderDisabled(String provider) {}
	}
	
	private void popUp(String message) 
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}
}
