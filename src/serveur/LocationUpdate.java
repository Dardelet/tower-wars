package serveur;
import structures.CurrentPlayer;
import android.content.Context;
import android.location.*;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;


public class LocationUpdate implements LocationListener
{
	private double lat;
	private double lng;
	private Context context;
	private Button actu;

	public LocationUpdate(Context context)
	{
		this.context = context;
	}
	public LocationUpdate(Context context, Button actu) 
	{
		this.context = context;
		this.actu = actu;
	}
	public void onLocationChanged(Location location) 
	    {
	         makeUseOfNewLocation(location);
	         //popUp("Mise à jour : "+lat+" "+ lng);
	    }

	    private void makeUseOfNewLocation(Location location) 
	    {
	    	lat = location.getLatitude();
			lng = location.getLongitude();
	        if(!CurrentPlayer.locUpdate)
	        {
	        	actu.setBackgroundColor(0xAA00FF00);
	        }
	        	CurrentPlayer.locUpdate = true;
			CurrentPlayer.posX = (int) ((int) 1000000*lat);
			CurrentPlayer.posY = (int) ((int) 1000000*lng);
			ServerThread server = new ServerThread(context);
			server.send(6, Integer.toString(CurrentPlayer.playerID), // L'instruction 6 est "mettreAJourJoueur"
					Integer.toString(CurrentPlayer.posX),
					Integer.toString(CurrentPlayer.posY),
					Integer.toString(CurrentPlayer.level),
					Integer.toString(CurrentPlayer.xpCount),
					"0",
					"0",
					"0");
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {}
	    public void onProviderEnabled(String provider) {}
	    public void onProviderDisabled(String provider) {}
	  
	    public void popUp(String message) 
		{
			Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		}
	    


}
