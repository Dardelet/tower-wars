package serveur;

import android.content.Context;
import android.widget.Toast;

public class NoResponseException extends Exception 
{
	private static final long serialVersionUID = 1L;

	public NoResponseException()
	{
		super();
	}
}
