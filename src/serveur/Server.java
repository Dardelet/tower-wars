package serveur;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import android.content.Context;

public class Server 
{	
	private Context context;
	private String reponse ;
	
	public Server(Context context)
	{
		this.context = context;
	}
	public String aSend(int instruction, String... parametres)
	{
		ATask task = new ATask(context, instruction, parametres);
		ServerThread.reponse = "-4";
		task.execute();
		return ServerThread.reponse;
	}
	
	public String send(int instruction, String... parametres)
	{
		int timeout = 5 ;
		Task task = new Task(instruction, parametres);
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<String> future = executor.submit(task);
		try 
		{
			reponse = future.get(timeout, TimeUnit.SECONDS);
		} 
		catch (TimeoutException e) 
		{
			return "-4";
		} 
		catch (InterruptedException e) 
		{
			return "-4";
		} 
		catch (ExecutionException e) 
		{
			return "-4";
		}
		executor.shutdownNow();
		
		return reponse;
	}
}