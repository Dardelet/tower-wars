package serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Callable;

public class Task implements Callable<String>
{
	private static final String url = "http://perso.telecom-paristech.fr/~beillevair/"; // Requete GET, on adjoint du code xml à l'url
	private int instruction;
	private String[] parametres;
	private String xml;

	public Task(int instruction, String... parametres)
	{
		this.instruction = instruction;
		this.parametres = parametres;
	}

	@Override
	public String call()
	{
		send(instruction, parametres);
		String data = ""; 
		String line;
		URL server; 
		HttpURLConnection connection;
		BufferedReader reader;
		try
		{
			server = new URL(convertFormat(url+xml));
			connection = (HttpURLConnection) server.openConnection();

			InputStream in = connection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(in));

			while ((line = reader.readLine()) != null) 
			{
				data += line;
			}
			connection.disconnect();
			reader.close();			
		}
		catch (IOException e){e.printStackTrace();}
		//interrupt();
		return data;
	}

	public void send(int instruction, String... parametres) // On envoie une instruction et un nombre de paramètres.
	{
		String body = null;
		String type = null;
		switch(instruction)
		{
		case(1): 
			type = "nouveauJoueur";
		body = "<joueur"
				+ " name=\"" + parametres[0] + "\"" 
				+ " password=\"" + parametres[1] + "\"/>";
		break ;

		case(2):
			type = "nouvelleTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour blocs=\"" + parametres[1] + "\"/>";
		break;

		case(3): 
			type = "login";
		body = "<joueur"
				+ " name=\"" + parametres[0] + "\"" 
				+ " password=\"" + parametres[1] + "\"/>";
		break;

		case(4): 
			type = "obtenirInfosJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(5): 
			type = "obtenirInfosTour";
		body = "<tour id=\"" + parametres[0] + "\"/>";
		break;

		case(6):
			type = "mettreAJourJoueur";
		body = "<joueur" 
				+ " id=\"" + parametres[0] + "\""
				+ " latitude=\"" + parametres[1] + "\""
				+ " longitude=\"" + parametres[2] + "\""
				+ " level=\"" + parametres[3] + "\""
				+ " xp=\"" + parametres[4] + "\""
				+ " lvlABloc=\"" + parametres[5] + "\""
				+ " lvlDBloc=\"" + parametres[6] + "\""			
				+ " lvlRBloc=\"" + parametres[7] + "\""
				+ "/>";
		break;

		case(7):
			type = "joueurAttaqueTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(8):
			type = "supprimerJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(9):
			type = "obtenirMapJoueurs";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(10):
			type = "obtenirMapTours";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(11): 
			type = "logout";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(12):
			type = "mettreAJourOr";
		body = "<joueur" 
				+	" id=\"" + parametres[0] + "\""  
				+  " prix=\"" + parametres[1] + "\"/>"; 
		break;
		case(13):
			type = "mettreDansSakatour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour blocs=\"" + parametres[1] + "\"/>";
		break;			
		case(14):
			type = "poserTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(15):
			type = "getSakatour";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(16):
			type = "getTowers";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(17):
			type="joueurAttaqueTour";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;

		case(18):
			type= "tourAttaqueJoueur";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<tour id=\"" + parametres[1] + "\"/>";
		break;	

		case(19):
			type="supprimerTour";
		body = "<tour id=\"" + parametres[0] + "\"/>";
		break;

		case(20):
			type = "obtenirMapCoffres";
		body = "<joueur id=\"" + parametres[0] + "\"/>";
		break;

		case(21):
			type = "ouvrirCoffre";
		body = "<joueur id=\"" + parametres[0] + "\"/>"
				+ "<chest id=\"" + parametres[1] + "\"/>";
		break;

		}

		xml = "?xml=<action type=\"" + type + "\">" +
				body +  
				"</action>";;
	}

	private String convertFormat(String instructions) // Convertit certain carctère comme ' ' '<' '>' 
	{
		String newInstructions = "";
		char c;
		String newC;

		for (int i = 0 ; i < instructions.length() ; i++)
		{
			c = instructions.charAt(i);
			switch(c)
			{
			case '<': newC = "%3C";
			break;
			case '>': newC = "%3E";
			break;
			case ' ': newC = "%20";
			break;
			case '"': newC = "%22";	
			break;
			default : newC = Character.toString(c);
			break;
			}
			newInstructions += newC;
		}
		return newInstructions;

	}

}
