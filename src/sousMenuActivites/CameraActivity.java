package sousMenuActivites;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.provider.MediaStore;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CameraActivity extends Activity 
{
	private Button b;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera);	// On affiche le titre du menu et un bouton retour	 
		b = (Button) findViewById(R.id.bouton_c);
		
		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); 
		intent.putExtra("camerasensortype", 2);
		intent.putExtra("autofocus", true); 
		intent.putExtra("fullScreen", false); 
		intent.putExtra("showActionIcons", false);
		startActivity(intent);
		
		b.setOnClickListener(
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) // Si on clique sur retour
					{
						Intent result = new Intent(); 
						result.putExtra("Bouton", "camera"); // On dit au menu que l'on vient de l'activité "caméra"
						setResult(RESULT_OK, result); // Et que tout s'est bien passé
						finish(); // Appelle de la méthode onDestroy() qui enleve la vue de la pile. La suivant est forcément Menu
					}
				});
		popUp("OnCreate");
	}

	protected void onDestroy() 
	{
		super.onDestroy();
		popUp("onDestroy()");
	}

	protected void onStart() 
	{// C'est ici que l'on relancera la caméra
		super.onStart();
		b.setClickable(true);
		popUp("onStart()");
	}

	protected void onStop() 
	{// C'est ici que l'on stoppera l'utilisation de la caméra
		super.onStop();
		b.setClickable(false);
		popUp("onStop()");
	}

	protected void onResume() 
	{
		super.onResume();
		popUp("onResume()");
	}

	protected void onPause() 
	{
		super.onPause();
		popUp("onPause()");
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	public void popUp(String message) 
	{
		//Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}