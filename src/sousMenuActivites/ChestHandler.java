package sousMenuActivites;

import java.util.ArrayList;
import java.util.Hashtable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pact.towerwars.R;

import serveur.ErrorResponseFormatException;
import serveur.Server;
import android.content.Context;
import android.widget.Toast;

public class ChestHandler 
{
	private String idPlayer;
	private Context context;
	private GoogleMap map;
	private Hashtable<LatLng,Integer> idChests =  new Hashtable<LatLng,Integer>();
	
	public ChestHandler(int idPlayer, Context context, GoogleMap map)
	{
		this.idPlayer = Integer.toString(idPlayer);
		this.context = context;
		this.map = map;
	}
	
	public void getChests()
	{
		Marker marker;
		LatLng latLng;
		BitmapDescriptor image = BitmapDescriptorFactory.fromResource(R.drawable.tresor);
		Server server = new Server(context);
		String reponse = server.send(20, idPlayer);
		//String reponse = fakeServer();
		String[] chests = reponse.split("/");		
		try
		{
			int n = chests.length;
			if (n % 3 > 1)
				throw new ErrorResponseFormatException();
			if(reponse.compareTo("0") == 0 || reponse.compareTo("") == 0)
				n = 0;
			for(int i = 0 ; i < n/3 ; i++)
			{
				latLng = new LatLng(Double.parseDouble(chests[3*i+1])/1000000, Double.parseDouble(chests[3*i+2])/1000000);
				idChests.put(latLng, Integer.parseInt(chests[3*i]));
				marker =map.addMarker(new MarkerOptions()
				.position(latLng)
				.title("Ce coffre renferme gloire et richesse") 
				.icon(image)); 
				marker.setDraggable(false);
			}
		}
		catch(ErrorResponseFormatException e)
		{
			popUp("Erreur serveur :"+reponse);
		}
		catch(NumberFormatException e)
		{
			popUp("Erreur parse :"+reponse);
		}
	}

	private String fakeServer() 
	{
		//return "0";
		return "42/48841000/2343807/24/48831000/2345879";
	}
	
	private void popUp(String message) 
	{
		Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
	}

	public void open(LatLng latLngChest) 
	{
		latLngChest =new LatLng(Math.round(1000000*latLngChest.latitude)/1000000.0,Math.round(1000000*latLngChest.longitude)/1000000.0);
		//popUp("Il s'agit du coffre numéro "+idChests.get(latLngChest));
		int idChest = idChests.get(latLngChest);
		Server server = new Server(context);
		String reponse = server.send(21, idPlayer, ""+idChest);
		try
		{
			String[] rewards = reponse.split("/");
			int n = rewards.length;
			if (n != 2)
				throw new ErrorResponseFormatException();
			int xp = Integer.parseInt(rewards[0]);
			int gold = Integer.parseInt(rewards[1]);
			popUp("Tu remportes "+xp+" xp et "+gold+" or.");
		}
		catch(NumberFormatException e)
		{
			popUp("Erreur nombre :"+reponse);
		}
		catch(ErrorResponseFormatException e)
		{	
			if(reponse.compareTo("0") == 0)
				popUp("Tu es trop loin");
			else
				popUp("Erreur format :"+reponse);
		}
		
	}
}
