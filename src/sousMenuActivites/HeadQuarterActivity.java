package sousMenuActivites;

import java.util.ArrayList;

import main.MenuActivity;


import serveur.ErrorResponseFormatException;
import serveur.ServerThread;
import structures.Activity2;
import structures.CurrentPlayer;
import structures.CurrentTowers;
import structures.TowerList;
import structures.Towers;

import structures.Tower;
import ununsedClasses.NewPlayerActivity;

import headQuartersActivities.DisplayTowerActivity;
import headQuartersActivities.ManageRessourcesActivity;
import headQuartersActivities.ManageUpgradesActivity;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.view.View;

public class HeadQuarterActivity 
extends Activity
implements View.OnClickListener

{
	private Button playerState;
	private Button towers;
	private Button upgrades;
	private Button returnMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState); // Idem CameraActivity
		LinearLayout layout = (LinearLayout) LinearLayout.inflate(this, R.layout.activity_head_quarter, null);
		playerState = (Button) layout.findViewById(R.id.player_state);
		towers = (Button) layout.findViewById(R.id.towers);
		upgrades = (Button) layout.findViewById(R.id.upgrades);
		returnMenu = (Button) layout.findViewById(R.id.return_menu);

		setContentView(layout);

		playerState.setOnClickListener(this);
		towers.setOnClickListener(this);
		upgrades.setOnClickListener(this);
		returnMenu.setOnClickListener(this);
	}

	protected void onResume() 
	{
		super.onResume();
		playerState.setText("Gérer son personnage");
		towers.setText("Gérer ses tours");
		upgrades.setText("Gérer ses améliorations");

	}

	protected void onPause() 
	{
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.head_quarter, menu);
		return true;
	}

	

	@Override
	public void onClick(View v) 
	{
		Intent HQIntent ;
		switch(v.getId())
		{
		case R.id.player_state:
			HQIntent = new Intent(this, ManageRessourcesActivity.class);
			playerState.setText("Connexion...");
			startActivity(HQIntent); 
			break;

		case R.id.towers:
			HQIntent = new Intent(this, DisplayTowerActivity.class);
			towers.setText("Connexion...");
			TowerList.towers = getTowersFromServer();
			if (TowerList.towers.size() == 0)
			{
				popUp("Tu ne possèdes aucune tour.");
				towers.setText("Gérer ses tours");
			}
			else
				startActivity(HQIntent); 
			break;
			
		case R.id.upgrades:
			HQIntent = new Intent(this, ManageUpgradesActivity.class);
			upgrades.setText("Connexion...");
			startActivity(HQIntent);
			break;
			
		case R.id.return_menu:
			finish();
			break;
		}

	}

	
	private ArrayList<Tower> getTowersFromServer()
	{
		ServerThread server = new ServerThread(this);
		server.send(16, Integer.toString(CurrentPlayer.playerID));
		String reponse = server.takeANap();
		CurrentTowers ct = new CurrentTowers();
		try
		{	// A faire : Gestion des erreurs NullPointer
			/* Le serveur envoie *  pour séparer les tours posées des tours non posées
			 * le problème est que le caractère * est réservé pour les expressions rationnelles en JAVA
			 * On le remplace donc par un caractère non réservé : %
			 */
			reponse = reponse.replace("*","%");
			/* Si il n'y a rien à gauche ou à droite de %, on rajoute une tour fatice pour que le reste 
			 * fonctionne correctement. 
			 * Cette tour fatice sera supprimée à la fin
			 */
			int index = reponse.indexOf("%");
			int n = reponse.length();
			if (index == 0)
				reponse = "0/A"+reponse;
			if (index == n-1)
				reponse = reponse + "0/A/0/0/0";
			
			String[] typeTowers = reponse.split("%");
			if ( (typeTowers.length != 2) )
				throw new ErrorResponseFormatException();
			String[] sakatour = typeTowers[0].split("/");
			String [] towers = typeTowers[1].split("/");
			Tower tower = null;
			if ((sakatour.length % 2 != 0) || (towers.length % 5 != 0))
				throw new ErrorResponseFormatException();
			
			for (int i = 0 ; i < (sakatour.length / 2) ; i++)
			{
				tower = new Tower(
						sakatour[i*2+1], 
						Integer.parseInt(sakatour[i*2]), 
						0.0,
						0.0, 
						false, 
						CurrentPlayer.getPlayer());
				ct.addTowerToSakatour(tower);
			}
			for (int i = 0 ; i < (towers.length / 5) ; i++)
			{
				tower = new Tower(
						towers[i*5+1],
						Integer.parseInt(towers[i*5]), 
						Double.parseDouble(towers[i*5+3]),
						Double.parseDouble(towers[i*5+2]),
						true, 
						CurrentPlayer.getPlayer());
				ct.addTowerToTowers(tower);
			}	
		}
		catch (NumberFormatException e)
		{
			popUp("Problème format nombre :"+reponse);
			finish();
		} 
		catch (ErrorResponseFormatException e) 
		{
			popUp("Mauvais format de la réponse :" +reponse);
			finish();
		}
		ct.clean(); // On enlève les tours fatices (id = 0)
		return ct.getAll();
	}
	
	public void temporaire()
	{
		// En attendant d'avoir une classe qui gère la liste des tours posées par le jour, on en créé manuellement
				Tower tower1 = new Tower("DDDARARAR", 6, 514554, 5444654, true, CurrentPlayer.getPlayer());
				Tower tower2 = new Tower("DARARARA", 102, 51125416, 5163151, true, CurrentPlayer.getPlayer());
				Tower tower3 = new Tower("DDDDARARRAAR", 754168415, 542154, 87875445, true, CurrentPlayer.getPlayer());
				// Attention cela tronque la tour
				Tower tower4 = new Tower("RADRAD", 104, 0,0, false, CurrentPlayer.getPlayer());
				Tower tower5 = new Tower("RDARARDDDDA", 105, 0,0, false, CurrentPlayer.getPlayer());
				Tower tower6 = new Tower("DAADDDDRADA", 106, 0,0, false, CurrentPlayer.getPlayer());
				
				CurrentTowers ct = new CurrentTowers();
				ct.addTowerToTowers(tower1);
				ct.addTowerToTowers(tower2);
				ct.addTowerToTowers(tower3);
				ct.addTowerToSakatour(tower4);
				ct.addTowerToSakatour(tower5);
				ct.addTowerToSakatour(tower6);
				
				Towers towers  = new Towers(this);
				towers.save(ct);
	}
	
	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}


}
