package sousMenuActivites;

import headQuartersActivities.DisplayTowerActivity;

import java.util.ArrayList;

import main.NewAccountActivity;

import realiteAugmentee.Server;
import serveur.ErrorResponseFormatException;
import serveur.LocationUpdate;
import serveur.ServerThread;
import structures.Activity2;
import structures.CurrentPlayer;
import structures.CurrentTowers;
import structures.Player;
import structures.Tower;
import structures.TowerList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pact.towerwars.R;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.location.*;
import attack.AttackActivity;

@SuppressLint("NewApi")
public class MapActivity 
extends Activity 
implements View.OnClickListener, OnMapLongClickListener, OnMarkerClickListener, OnMenuItemClickListener
{
	private String provider;
	private Button quit;
	private Button actu;
	private GoogleMap mMap;
	private LocationManager locationManager;
	private double lat = 0;
	private double lng = 0;
	private LocationUpdate locationListener;
	private RelativeLayout layout;
	private int tourAPoser = 0;
	private ChestHandler chests;
	private boolean ourTower = false;
	private String idTourMenuDestroy;
	private Marker chest;

	protected void onStart()
	{
		super.onStart(); // Active l'update des coordonnées GPS toutes les 10 secondes
		//locationListener = new LocationUpdate(this, actu);
		//locationManager.requestLocationUpdates(provider, 5000, 0, locationListener);
		getMap();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		CurrentPlayer.load(this);
		super.onCreate(savedInstanceState); // Idem HeadQuarterActivity
		layout = (RelativeLayout) RelativeLayout.inflate(this, R.layout.activity_map, null);
		quit = (Button) layout.findViewById(R.id.bouton_m);
		actu = (Button) layout.findViewById(R.id.bouton_actualiser);
		actu.setBackgroundColor(0xAAFF0000);

		locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		ArrayList<LocationProvider> providers = new ArrayList<LocationProvider>();
		ArrayList<String> names = (ArrayList<String>) locationManager.getProviders(true);
		for(String name : names)
		{providers.add(locationManager.getProvider(name));}

		Criteria critere = new Criteria();
		// Pour indiquer la pr��cision voulue
		// On peut mettre ACCURACY_FINE pour une haute pr��cision ou ACCURACY_COARSE pour une moins bonne pr��cision
		critere.setAccuracy(Criteria.ACCURACY_FINE);

		// Est-ce que le fournisseur doit ��tre capable de donner une altitude ?
		critere.setAltitudeRequired(false);

		// Est-ce que le fournisseur doit ��tre capable de donner une direction ?
		critere.setBearingRequired(false);

		// Est-ce que le fournisseur peut ��tre payant ?
		critere.setCostAllowed(false);

		// Pour indiquer la consommation d'��nergie demand��e
		// Criteria.POWER_HIGH pour une haute consommation, Criteria.POWER_MEDIUM pour une consommation moyenne et Criteria.POWER_LOW pour une basse consommation
		critere.setPowerRequirement(Criteria.POWER_HIGH);

		// Est-ce que le fournisseur doit ��tre capable de donner une vitesse ?
		critere.setSpeedRequired(false);


		mMap=((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMyLocationEnabled(true);
		chests = new ChestHandler(CurrentPlayer.playerID, this, mMap);
		provider=locationManager.getBestProvider(critere, true);
		Location location = locationManager.getLastKnownLocation(provider);
		if(location != null)
		{
			lat = location.getLatitude();
			lng = location.getLongitude();
		}

		CurrentPlayer.gps.setButton(actu);
		if(CurrentPlayer.locUpdate)
			actu.setBackgroundColor(0xAA00FF00);
		mMap.setOnMapLongClickListener(this);
		mMap.setOnMarkerClickListener(this);
		//GeoPoint point = new GeoPoint(19240000,-99120000);
		setContentView(layout);
		quit.setOnClickListener(this);
		actu.setOnClickListener(this);
		Intent i = getIntent();
		tourAPoser = i.getIntExtra("TourAPoser", 0);
		popUp("Recherche de la position, patientez puis appuyez sur Actualiser");
	}
	
	private void displayMenuTower()
	{
		TextView v = (TextView) findViewById(R.id.center);
		PopupMenu popup = new PopupMenu(this, v);
		popup.getMenu().add(Menu.NONE, 1, Menu.NONE, "Attaquer");
		popup.getMenu().add(Menu.NONE, 2, Menu.NONE, "Inspecter");
		if(ourTower)
		{
			popup.getMenu().add(Menu.NONE, 3, Menu.NONE, "Détruire");
			ourTower = false;
		}	
        popup.setOnMenuItemClickListener(this);
        popup.show();
    }
	
	private void displayMenuChest()
	{
		TextView v = (TextView) findViewById(R.id.center);
		PopupMenu popup = new PopupMenu(this, v);
		popup.getMenu().add(Menu.NONE, 4, Menu.NONE, "Ouvrir");
		popup.setOnMenuItemClickListener(this);
        popup.show();
	}

	private void getMap() 
	{
		mMap.clear();
		LatLng locActuelle = new LatLng(lat, lng);
		mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(locActuelle,17));
		Marker marker;
		LatLng latLng = null;
		ServerThread server = new ServerThread(this);
		server.send(10, Integer.toString(CurrentPlayer.playerID));
		String towers = server.takeANap();
		server = new ServerThread(this);
		server.send(9, Integer.toString(CurrentPlayer.playerID));
		String players = server.takeANap();
		String[] towersArray = towers.split("/");
		String[] playersArray = players.split("/");
		try
		{
			if (towersArray.length % 7 > 1)
				throw new ErrorResponseFormatException();
			if (playersArray.length % 11 > 1)
				throw new ErrorResponseFormatException();				
		}
		catch(ErrorResponseFormatException e)
		{
			//popUp("Erreur serveur : "+players + " +"+towers);
			popUp(""+(towersArray[0]));
			popUp(towers);
			popUp(players);
		}
		for(int i = 0 ;  i < (towersArray.length)/7 ; i++)
		{
			latLng = new LatLng(Double.parseDouble(towersArray[7*i+3])/1000000, Double.parseDouble(towersArray[7*i+2])/1000000);
			BitmapDescriptor image = BitmapDescriptorFactory.fromResource(R.drawable.tour);
			if(Integer.parseInt(towersArray[7*i+5]) != CurrentPlayer.playerID)
				image = BitmapDescriptorFactory.fromResource(R.drawable.tourouge);
			marker =mMap.addMarker(new MarkerOptions()
			.position(latLng)
			.title("Propriété de : "+towersArray[7*i +6]+" / Id de la tour : n°" +towersArray[7*i]) 
			.icon(image)); 
			marker.setDraggable(false);
			//		new Tower(towersArray[6*i+0], towersArray[6*i+1], towersArray[6*i+2], towersArray[6*i+3], towersArray[6*i+4], towersArray[6*i+5]);
		}
		for(int i = 0 ;  i < (playersArray.length)/11 ; i++)
		{
			if(Integer.parseInt(playersArray[11*i]) != CurrentPlayer.playerID)
			{
				try
				{
					latLng = new LatLng(Double.parseDouble(playersArray[11*i+3])/1000000, Double.parseDouble(playersArray[11*i+2])/1000000);
				}
				catch(NumberFormatException e)
				{
					popUp("Erreur serveur :"+players);
				}
				marker =mMap.addMarker(new MarkerOptions()
				.position(latLng)
				.title("Joueur : "+playersArray[11*i+1]+"( id n°" + playersArray[11*i] +")") 
				.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher))); 
				marker.setDraggable(false);
			}
		}
		chests.getChests();
		
		actu.setText("Actualiser");
	}

	@Override
	public void onMapLongClick(LatLng coordinates) 
	{
		if(tourAPoser == 0)
		{
			ServerThread server = new ServerThread(this);
			server.send(16, Integer.toString(CurrentPlayer.playerID));
			String reponse = server.takeANap();
			CurrentTowers ct = new CurrentTowers();
			try
			{	// A faire : Gestion des erreurs NullPointer
				/* Le serveur envoie *  pour séparer les tours posées des tours non posées
				 * le problème est que le caractère * est réservé pour les expressions rationnelles en JAVA
				 * On le remplace donc par un caractère non réservé : %
				 */
				reponse = reponse.replace("*","%");
				/* Si il n'y a rien à gauche ou à droite de %, on rajoute une tour fatice pour que le reste 
				 * fonctionne correctement. 
				 * Cette tour fatice sera supprimée à la fin
				 */
				int index = reponse.indexOf("%");
				int n = reponse.length();
				if (index == 0)
					reponse = "0/A"+reponse;
				if (index == n-1)
					reponse = reponse + "0/A/0/0/0";
	
				String[] typeTowers = reponse.split("%");
				if ( (typeTowers.length != 2) )
					throw new ErrorResponseFormatException();
				String[] sakatour = typeTowers[0].split("/");
				String [] towers = typeTowers[1].split("/");
				Tower tower = null;
				if ((sakatour.length % 2 != 0) || (towers.length % 5 != 0))
					throw new ErrorResponseFormatException();
	
				for (int i = 0 ; i < (sakatour.length / 2) ; i++)
				{
					tower = new Tower(
							sakatour[i*2+1], 
							Integer.parseInt(sakatour[i*2]), 
							0.0,
							0.0, 
							false, 
							CurrentPlayer.getPlayer());
					ct.addTowerToSakatour(tower);
				}
				for (int i = 0 ; i < (towers.length / 5) ; i++)
				{
					tower = new Tower(
							towers[i*5+1],
							Integer.parseInt(towers[i*5]), 
							Double.parseDouble(towers[i*5+3]),
							Double.parseDouble(towers[i*5+2]),
							true, 
							CurrentPlayer.getPlayer());
					ct.addTowerToTowers(tower);
				}
				ct.clean();
	
				if((ct == null) ||(ct.getSakatour() == null) || ct.getSakatour().isEmpty())
				{
					popUp("Aucune tour n'a été trouvée dans le Sakatour");
				}
				else
				{
					TowerList.towers = ct.getSakatour();
					Intent displayTower = new Intent(this, DisplayTowerActivity.class);
					displayTower.putExtra("Lancé par MapActivity", 1);
					startActivityForResult(displayTower, 1);
				}
			}
			catch(Exception e){}
		}
		else
			onActivityResult(0,0,null);
	}	

	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		int idTour;
		if(data != null)
			 idTour = data.getIntExtra("idTour",0);
		else
			idTour = tourAPoser;
		if(idTour != 0 && CurrentPlayer.locUpdate)
		{
			ServerThread server = new ServerThread(this);
			server.send(14, Integer.toString(CurrentPlayer.playerID), Integer.toString(idTour));
			String reponse = server.takeANap();
			int codeReponse;
			if (reponse == null)
				codeReponse = -4;
			else
			{
				try
				{
					codeReponse = Integer.parseInt(reponse);
				}
				catch (NumberFormatException e)
				{
					codeReponse = -4;
				}
			}
			if (codeReponse < 0)
			{
				if ((codeReponse == -4) || (codeReponse == -1))
					popUp("Erreur serveur");
				if (codeReponse == -2)
					popUp("Tu n'as pas assez d'argent");
				if (codeReponse == -3)
					popUp("Tu es trop proche d'une tour existante");
			}
			else 
				getMap();
		}
		if(!(CurrentPlayer.locUpdate))
			popUp("Mauvaise Couverture GPS, patientez");
	}
	public boolean onMarkerClick(Marker arg0) 
	{
		String titre = arg0.getTitle();
		switch(titre.charAt(0))
		{
		case 'J':
			int index = titre.indexOf("(");
			String nom = titre.substring(9, index);
			popUp(nom+" te regarde l'air menaçant");
			break;
		case 'C':
			chest = arg0;
			displayMenuChest();
			break;
		default:
			String id = titre.substring(titre.indexOf("°")+1);
			ServerThread server = new ServerThread(this);
			server.send(5, id);
			String info = server.takeANap();
			String[] infos = info.split("/");
			try
			{
				if(infos.length != 5)
					throw new ErrorResponseFormatException();
				ServerThread server2 = new ServerThread(this);
				server2.send(4, infos[3]);
				String reponse = server2.takeANap();
				String[] infos2 = reponse.split("/");
				String name = infos2[0];
				Player player = new Player(name);
				player.setPlayerID(Integer.parseInt(infos[3]));
				Tower tour = new Tower(infos[0], 
						Integer.parseInt(id), 
						Double.parseDouble(infos[2])/1000000, 
						Double.parseDouble(infos[1])/1000000,
						true,
						player);
				ArrayList<Tower> tours = new ArrayList<Tower>();
				tours.add(tour);
				TowerList.towers = tours;
				TowerList.towerAttack = tour;
				LatLng latLng = new LatLng(tour.getPosX(),tour.getPosY());
				drawCircle(latLng);
				
				if(infos[3].compareTo(""+CurrentPlayer.playerID) == 0)
				{
					ourTower = true;
					idTourMenuDestroy = id;
				}
				displayMenuTower();
			}
			catch(ErrorResponseFormatException e)
			{
				popUp("Erreur Serveur :"+info);
				finish();
			}
			break;
			
		}
		return false;

	}

	private void drawCircle(LatLng latLng) 
	{
		mMap.addCircle(new CircleOptions()
	     .center(latLng)
	     .radius(200)
	     .strokeColor(Color.RED)
	     .fillColor(Color.TRANSPARENT));		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.bouton_m:
			finish();
			break;
		case R.id.bouton_actualiser:
			if (CurrentPlayer.locUpdate)
			{
				actu.setText("Patientez..");
				lat = ((double)CurrentPlayer.posX)/1000000.0;
				lng = ((double)CurrentPlayer.posY)/1000000.0;
				getMap();					
			}
			else 
				popUp("Mauvaise Couverture GPS, patientez");
			break;
		}
	}
	
	protected void onStop() // Quand on quitte la carte, on désactive l'update régulières des coordonnées au serveur
	{
		super.onStop();
		//locationManager.removeUpdates(locationListener);
		//locationListener = null;
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) 
	{
		Intent intent;
		switch (item.getItemId()) 
		{
		case 1:
			intent = new Intent(this, AttackActivity.class);
			startActivity(intent);
			break;
		case 2:
			intent = new Intent(this, Modtour.class);
			startActivity(intent);
			break;
		case 3:
			Server server = new Server(this);
			server.send(19, idTourMenuDestroy);
			getMap();
			break;
		case 4:
			LatLng latLngChest = chest.getPosition();
			chests.open(latLngChest);
			getMap();
			break;
		}
		return false;
	}

}
