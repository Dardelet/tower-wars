package sousMenuActivites;


import structures.Tower;
import structures.TowerList;

import com.pact.towerwars.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;



public class Modtour extends Activity implements View.OnClickListener{

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		RelativeLayout layout = (RelativeLayout) RelativeLayout.inflate(this, R.layout.activity_modtour, null);

		Button returnMap = (Button) layout.findViewById(R.id.return_map);
		Button attack = (Button) layout.findViewById(R.id.attack);
		attack.setVisibility(View.INVISIBLE);
		TextView ownerTower = (TextView) layout.findViewById(R.id.owner_tower);
		TextView blocTower = (TextView) layout.findViewById(R.id.bloc_tower);

		Tower tour = TowerList.towers.get(0);
		ownerTower.setText(tour.getPlayer().getPlayerName() + "(id n° "+ tour.getPlayer().getPlayerID()+")");
		blocTower.setText(tour.getTower());
		setContentView(layout);
		returnMap.setOnClickListener(this);
		attack.setOnClickListener(this);
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.modtour, menu);
		return true;
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId()) // Sur quel bouton avons nous cliqué ?

		{ // Pour chaque bouton, on lance l'activité correspondante
		case R.id.return_map:
			finish();
			break;
		case R.id.attack:
			attaquerTour();
			break;
		}
	}

	private void attaquerTour() 
	{
		popUp("La tour est attaquée !");
	}
	
	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
