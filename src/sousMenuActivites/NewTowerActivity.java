package sousMenuActivites;

import com.pact.towerwars.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class NewTowerActivity extends Activity 
{
	private Button b;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState); // Idem MapActivity
		setContentView(R.layout.activity_new_tower);
		b = (Button) findViewById(R.id.bouton_nt);
		b.setOnClickListener(
				new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						Intent result = new Intent();
						result.putExtra("Bouton", "nouvelle tour");
						setResult(RESULT_OK, result);
						finish();
					}
				});
		popUp("OnCreate");
	}

	protected void onDestroy() 
	{
		super.onDestroy();
		popUp("onDestroy()");
	}

	protected void onStart() 
	{
		super.onStart();
		b.setClickable(true);
		popUp("onStart()");
	}

	protected void onStop() 
	{
		super.onStop();
		b.setClickable(false);
		popUp("onStop()");
	}

	protected void onResume() 
	{
		super.onResume();
		popUp("onResume()");
	}

	protected void onPause() 
	{
		super.onPause();
		popUp("onPause()");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_tower, menu);
		return true;
	}

	public void popUp(String message) 
	{
		//Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
