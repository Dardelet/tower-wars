package structures;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;

public class Activity2 extends Activity
{
	private ProgressDialog progressDialog;

	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		requestWindowFeature(Window.FEATURE_PROGRESS);
	}
	
	protected void spinnerOn()
	{
		setProgressBarIndeterminateVisibility(true);
	}
	
	protected void spinnerOff()
	{
		setProgressBarIndeterminateVisibility(false);
	}
	
	protected void on()
	{
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            progressDialog = new ProgressDialog(this);
        } 
		else {
            progressDialog = new ProgressDialog(this,
                    AlertDialog.THEME_HOLO_LIGHT);
        }
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Connexion ...");
        progressDialog.show();
	}
	
	protected void off()
	{
		progressDialog.dismiss();
	}
}
