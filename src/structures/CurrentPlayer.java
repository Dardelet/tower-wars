package structures;

import serveur.GPS;
import android.content.Context;
import android.widget.Toast;


public class CurrentPlayer // Etat du joueur courant, les attributs sont publics pour une meilleure accessibilité
{
	public static String playerName; // Nom du joueur
	public static int playerID; // Correspondance bijective entre un nom de joueur et un entier
	public static int posX; // Position GPS du joueur
	public static int posY;
	public static int level;
	public static int lifePoints; // son niveau (se calcule avec xpCount)
	public static int xpCount; // son nombre d'xp 
	public static long goldAmount; // Quantité de ressource "gold" que possède le joueur
	public static int lvlABloc; // Niveau d'amélioration du bloc d'attaque
	public static int lvlDBloc; // 					   du bloc défense
	public static int lvlRBloc; //					   du bloc ressource
	public static boolean locUpdate = false;
	public static GPS gps;
	public static void setPlayer(Player player)
	{
		CurrentPlayer.playerName = player.playerName;
		CurrentPlayer.playerID = player.playerID;
		CurrentPlayer.posX = player.posX;
		CurrentPlayer.posY = player.posY;
		CurrentPlayer.level = player.level;
		CurrentPlayer.lifePoints = player.lifePoints;
		CurrentPlayer.xpCount = player.xpCount;
		CurrentPlayer.goldAmount = player.goldAmount;
		CurrentPlayer.lvlABloc = player.lvlABloc;
		CurrentPlayer.lvlDBloc = player.lvlDBloc;
		CurrentPlayer.lvlRBloc = player.lvlRBloc;
	}
	
	public static void initialiser(String playerName)
	{
		CurrentPlayer.playerName = playerName; 
		playerID = -1; 
		posX = 0;
		posY = 0;
		level = 1;
		lifePoints = 100; 
		xpCount = 0; 
		goldAmount = 100 ;
		lvlABloc = 1;
		lvlDBloc = 1;
		lvlRBloc = 1;
	}

	public static void createPlayer (String playerName, int playerID, int posX, int posY, int level, int lifePoints, int xpCount, int goldAmount, int lvlABloc, int lvlDBloc, int lvlRBloc)
	{
		CurrentPlayer.playerName = playerName;
		CurrentPlayer.playerID = playerID;
		CurrentPlayer.posX = posX;
		CurrentPlayer.posY = posY;
		CurrentPlayer.level = level;
		CurrentPlayer.lifePoints = lifePoints;
		CurrentPlayer.xpCount = xpCount;
		CurrentPlayer.goldAmount = goldAmount;
		CurrentPlayer.lvlABloc = lvlABloc;
		CurrentPlayer.lvlDBloc = lvlDBloc;
		CurrentPlayer.lvlRBloc = lvlRBloc;
	}
	
	public static int calculateMaxHeight() 
	{
		return (int) (4 + Math.floor(level/2));
	}
	
	public static int getMaxLife()
	{
		return 100;
	}

	public static void save(Context context)
	{
		Players players = new Players(context);
		players.saveCurrentPlayer();
	}
	
	public static void load(Context context)
	{
		Players players = new Players(context);
		players.getCurrentPlayer();
	}

	public static Player getPlayer()
	{
		return new Player(
				CurrentPlayer.playerName,
				CurrentPlayer.playerID,
				CurrentPlayer.posX,
				CurrentPlayer.posY,
				CurrentPlayer.level,
				CurrentPlayer.lifePoints,
				CurrentPlayer.xpCount,
				CurrentPlayer.goldAmount,
				CurrentPlayer.lvlABloc,
				CurrentPlayer.lvlDBloc,
				CurrentPlayer.lvlRBloc);
	}

	public static int xpCountMax() // Calcule le niveau d'expérience requis pour passer au niveau supérieur 
	// en fonction du niveau actuel
	{
		int m ;
		int a = 100 ;
		int b = 0;
		m = a*(level) + b ;
		return m;
	}


	public static void setXpCount(Context context, int extraXpCount) // Ajoute de l'xp au jour
	// gère les augmentations de niveaux
	{	
		xpCount += extraXpCount;
		while (xpCount >= xpCountMax())
		{
			xpCount = xpCount - xpCountMax() ;
			level ++ ;
			lvlUpNotification(context);
		}
	}
	
	public static void lvlUpNotification(Context context) // Affiche une augmentation de niveau
	{
		Toast.makeText(context, "Level Up ! Tu es niveau " + Integer.toString(level), Toast.LENGTH_SHORT).show();
	}




}
