package structures;

import java.io.Serializable;
import java.util.ArrayList;

public class CurrentTowers // Une classe encapsulant 2 listest de tours
	implements Serializable
{
	private ArrayList<Tower> sakatour; // Liste de modèles de tours créés dans "Construction de tour" en attente d'être posées
	private ArrayList<Tower> towers; // Liste des tours posées
	
	public CurrentTowers()
	{
		sakatour = new ArrayList<Tower>();
		towers = new ArrayList<Tower>();
	}

	public void update(Tower tower)
	{
		int id = tower.getId();
		for(int i = 0 ; i < sakatour.size();i++)
		{
			Tower tour = sakatour.get(i);
			if (tour.getId() == id)
			{
				sakatour.remove(tour);
				towers.add(tour);
			}
		}
				
		
	}
	
	public void addTowerToSakatour(Tower tower) 
	{
		sakatour.add(tower);
	}
	
	public void addTowerToTowers(Tower tower)
	{
		towers.add(tower);
	}
	
	public ArrayList<Tower> getSakatour()
	{
		return sakatour;
	}
	
	public ArrayList<Tower> getTowers()
	{
		return towers;
	}
	
	public ArrayList<Tower> getAll()
	{
		ArrayList<Tower> all = sakatour;
		all.addAll(towers);
		return all;
	}

	public Tower getTowerFromId(int id) 
	{
		Tower tour = null;
		for(Tower tower : sakatour)
			if (tower.getId() == id)
				tour = tower;
		for(Tower tower : towers)
			if (tower.getId() == id)
				tour = tower;
		
		return tour;
		
	}

	public void clean() 
	{
		for(Tower tour : sakatour)
			if(tour.getId() == 0)
				sakatour.remove(tour);
		for(Tower tour : towers)
			if(tour.getId() == 0)
				towers.remove(tour);
	}
}
