package structures;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.Toast;


public class Player // Représente un joueur
	implements Serializable // On implémente cette interface pour pouvoir se passer un Player entre activités
{
	private static final long serialVersionUID = 1L;
	public String playerName; // Nom du joueur
	public int playerID; // Correspondance bijective entre un nom de joueur et un entier
	public int posX; // Position GPS du joueur
	public int posY;
	public int level;
	public int lifePoints; // son niveau (se calcule avec xpCount)
	public int xpCount; // son nombre d'xp 
	public long goldAmount; // Quantité de ressource "gold" que possède le joueur
	public int lvlABloc; // Niveau d'amélioration du bloc d'attaque
	public int lvlDBloc; // 					   du bloc défense
	public int lvlRBloc; //					   du bloc ressource
	
	public Player(String playerName)
	{
		this.playerName = playerName; // On initialise tous les paramètres
		playerID = -1; // Normalement, l'iD est initialisé juste après la création d'un joueur
		posX = 0; // En attendant de demander au GPS, on est en 0,0
		posY = 0;
		level = 1;// On commence niveau 1
		lifePoints = 100; // On a toujours 100 points de vie quel que soit l'avancement dans le jeu
		xpCount = 0; // On commence avec un niveau d'xp nul
		goldAmount = 100 ; // On commence avec 100 "or"
		lvlABloc = 1; // Niveau d'amélioration des Bloc 
		lvlDBloc = 1;
		lvlRBloc = 1;
	}
	
	public void initialiser(String playerName)
	{
		this.playerName = playerName; 
		playerID = -1; 
		posX = 0;
		posY = 0;
		level = 1;
		lifePoints = 100; 
		xpCount = 0; 
		goldAmount = 100 ;
		lvlABloc = 1;
		lvlDBloc = 1;
		lvlRBloc = 1;
	}
	
	public  Player (String playerName, int playerID, int posX, int posY, int level, int lifePoints, int xpCount, long goldAmount, int lvlABloc, int lvlDBloc, int lvlRBloc)
	{
		this.playerName = playerName;
		this.playerID = playerID;
		this.posX = posX;
		this.posY = posY;
		this.level = level;
		this.lifePoints = lifePoints;
		this.xpCount = xpCount;
		this.goldAmount = goldAmount;
		this.lvlABloc = lvlABloc;
		this.lvlDBloc = lvlDBloc;
		this.lvlRBloc = lvlRBloc;
	}

	
	
	public Player() {
		// TODO Auto-generated constructor stub
	}

	public int calculateMaxHeight() 
	{
		// en fonction du niveau du joueur, on peut construire des tours plus ou moins haute.
		// La formule n'est pas encore implémentée
		return 5;
	}
	

	
	public int xpCountMax() // Calcule le niveau d'expérience requis pour passer au niveau supérieur 
							// en fonction du niveau actuel
	{
		int m ;
		int a = 100 ;
		int b = 0;
		m = a*(level) + b ;
		return m;
	}
	
	
	public void setXpCount(int extraXpCount) // Ajoute de l'xp au jour
											// gère les augmentations de niveaux
	{	
		xpCount += extraXpCount;
			while (xpCount >= xpCountMax())
			{
				xpCount = xpCount - xpCountMax() ;
				level ++ ;
			}
	}
	
	
	/*@Override
	public int describeContents() // Les deux méthodes suivantes sont pour l'interface Parcelable
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
	    dest.writeString(playerName);
	    dest.writeInt(posX);
	    dest.writeInt(posY);
	    dest.writeInt(level);
	    dest.writeInt(xpCount);
	    dest.writeInt(goldAmount);
	    dest.writeInt(lvlABloc);
	    dest.writeInt(lvlDBloc);
	    dest.writeInt(lvlRBloc);
	}*/

	public String getPlayerName() 
	{
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public int getPlayerID() {
		return playerID;
	}

	public void setPlayerID(int playerID) {
		this.playerID = playerID;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLifePoints() {
		return lifePoints;
	}

	public void setLifePoints(int lifePoints) {
		this.lifePoints = lifePoints;
	}

	public long getGoldAmount() {
		return goldAmount;
	}

	public void setGoldAmount(long goldAmount) {
		this.goldAmount = goldAmount;
	}

	public int getLvlABloc() {
		return lvlABloc;
	}

	public void setLvlABloc(int lvlABloc) {
		this.lvlABloc = lvlABloc;
	}

	public int getLvlDBloc() {
		return lvlDBloc;
	}

	public void setLvlDBloc(int lvlDBloc) {
		this.lvlDBloc = lvlDBloc;
	}

	public int getLvlRBloc() {
		return lvlRBloc;
	}

	public void setLvlRBloc(int lvlRBloc) {
		this.lvlRBloc = lvlRBloc;
	}

	public int getXpCount() {
		return xpCount;
	}





}
