package structures;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import structures.CurrentPlayer;

import com.pact.towerwars.R;
import com.pact.towerwars.R.layout;
import com.pact.towerwars.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.view.Menu;

public class Players // Cette classe gère la sauvegarde et le chargement d'un joueur en mémoire.
// Toutes les classes en commentaires sont en stand-by. Elles étaient utilisées pour sauvegarder une correspondance
// joueur - id, mais s'est avérée très peu utile.
{
//	private String PLAYERS = "playerList.txt";
	private static String PLAYER = "player.txt";
	private Context context;

	public Players(Context context)
	{
		this.context = context;
	}


	/*public void deleteAllPlayers()
	{
		context.deleteFile(PLAYERS);
	}*/

	/*public void deletePlayer(String playerName)
	{
		String playerList = getPlayerList();
		String player = ""; 
		String playerList3 = "";
		ArrayList<String> playerList2 = new ArrayList<String>();
		int index = 0;

		for(int i = 0 ; i< playerList.length() ; i++)
		{
			if (playerList.charAt(i) == '/')
			{
				playerList2.add(player);
				player = "";
				continue;
			}
			player += playerList.charAt(i);		
		}

		for(String playerId : playerList2)
		{
			index = playerId.indexOf("=");
			if (playerId.substring(0,index).compareTo(playerName) == 0)
			{
				playerList2.remove(playerId);
				break;
			}
		}

		for (String playerId : playerList2)
			playerList3 += playerId+"/";

		setPlayerList(playerList3);
	}*/

	/*private boolean setPlayerList(String playerList3) 
	{
		boolean ok = false;
		FileOutputStream output = null;   

		try {
			output = context.openFileOutput(PLAYERS, Context.MODE_PRIVATE);
			output.write(playerList3.getBytes());
			if(output != null)
				output.close();
			ok = true ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ok;
	}*/

	/*public boolean initialiserPlayers(String player, int id)
	{
		deleteAllPlayers();
		boolean ok = false;
		FileOutputStream output = null;   
		String init = player + "=" + Integer.toString(id) + "/";

		try {
			output = context.openFileOutput(PLAYERS, Context.MODE_PRIVATE);
			output.write(init.getBytes());
			if(output != null)             android:screenOrientation="portrait" >
           android:screenOrientation="portrait" >

				output.close();
			ok = true ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ok;

	}*/

	/*public String getPlayerList()
	{
		FileInputStream input = null;   

		StringBuffer lu = new StringBuffer();
		try {
			int value;
			input = context.openFileInput(PLAYERS);
			while((value = input.read()) != -1) {
				lu.append((char)value);
			}
			if(input != null)
				input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return lu.toString();
	}*/

	/*public boolean addPlayer(String player, int id)
	{
		boolean ok = false;
		FileOutputStream output = null;   
		String newPlayer = player + "=" + Integer.toString(id) + "/";
		String newList = getPlayerList() + newPlayer;

		try {
			output = context.openFileOutput(PLAYERS, Context.MODE_PRIVATE);
			output.write(newList.getBytes());
			if(output != null)
				output.close();
			ok = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		return ok;
	}*/

	/*public String getPlayerFromId(int id)
	{
		String playerList = getPlayerList();
		String player = ""; 
		ArrayList<String> playerList2 = new ArrayList<String>();
		int index = 0;

		for(int i = 0 ; i< playerList.length() ; i++)
		{
			if (playerList.charAt(i) == '/')
			{
				playerList2.add(player);
				player = "";
				continue;
			}
			player += playerList.charAt(i);		
		}

		for(String playerId : playerList2)
		{
			index = playerId.indexOf("=");
			if (playerId.substring(index+1).compareTo(Integer.toString(id)) == 0)
			{
				player = playerId.substring(0, index);
				break;
			}
		}

		if (player.compareTo("") == 0)
			return null;
		else 
			return player;		
	}*/

	/*public int getIdFromPlayer(String playerName) // On suppose que tout le monde a des prénoms différents
	{
		String playerList = getPlayerList();
		String player = ""; 
		int id = 0;
		ArrayList<String> playerList2 = new ArrayList<String>();
		int index = 0;

		for(int i = 0 ; i< playerList.length() ; i++)
		{
			if (playerList.charAt(i) == '/')
			{
				playerList2.add(player);
				player = "";
				continue;
			}
			player += playerList.charAt(i);		
		}

		for(String playerId : playerList2)
		{
			index = playerId.indexOf("=");
			if (playerId.substring(0,index).compareTo(playerName) == 0)
			{
				id = Integer.parseInt(playerId.substring(index+1));
				break;
			}
		}

		return id; // 0 si non trouvé, id sinon (un id est toujours >0)		
	}*/

	public String findCurrentPlayerName() 
	{
		/*FileInputStream input = null;   
		StringBuffer lu = new StringBuffer();
		//this.deleteFile("player.txt");
		try {
			int value;
			input = context.openFileInput(PLAYER);
			while((value = input.read()) != -1) {
				lu.append((char)value);
			}
			if(input != null)
				input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// Cherche dans la mémoire du téléphone (dans le Bundle) si une partie a déjà été commencée
		// Si oui on renvoit le nom du joueur, si non on renvoit ""
		String mot = lu.toString();
		if (mot.length() == 0)
			return mot;
		else
		{
			String reponse = "";
			int i = 0;
			while(mot.charAt(i) != '/' )
			{
				reponse += mot.charAt(i);
				i++;
			}*/
		
		Player player = null; 
		try
		{
			FileInputStream fis = context.openFileInput(PLAYER);
			ObjectInputStream ois = new ObjectInputStream(fis);		
			player = (Player) ois.readObject();
			ois.close();
		}
		catch (FileNotFoundException e) {e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();} 
		catch (ClassNotFoundException e) {e.printStackTrace();}
		if (player == null)
			return "";
		if (player.getPlayerName() == null)
			return "";
		return player.getPlayerName();
	}

	public void saveCurrentPlayer() 
	{
		/*String[] savedPlayer = new String[11]; 
		savedPlayer[0] = CurrentPlayer.playerName;
		savedPlayer[1] = Integer.toString(CurrentPlayer.playerID); 
		savedPlayer[2] = Integer.toString(CurrentPlayer.posX); 
		savedPlayer[3] = Integer.toString(CurrentPlayer.posY);
		savedPlayer[4] = Integer.toString(CurrentPlayer.level);
		savedPlayer[5] = Integer.toString(CurrentPlayer.lifePoints); 
		savedPlayer[6] = Integer.toString(CurrentPlayer.xpCount); 
		savedPlayer[7] = Integer.toString(CurrentPlayer.goldAmount);
		savedPlayer[8] = Integer.toString(CurrentPlayer.lvlABloc);
		savedPlayer[9] = Integer.toString(CurrentPlayer.lvlDBloc);
		savedPlayer[10] = Integer.toString(CurrentPlayer.lvlRBloc);

		String savedPlayer2 = "";
		for(int i = 0 ; i < 11 ; i++)
			savedPlayer2 += savedPlayer[i] + "/";

		FileOutputStream output = null;        
		boolean ok = false;
		try {
			output = context.openFileOutput(PLAYER, Context.MODE_PRIVATE);
			output.write(savedPlayer2.getBytes());
			if(output != null)
				output.close();
			ok = true ;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		*/
		
		FileOutputStream fos;
		ObjectOutputStream oos;
		try 
		{
			fos = context.openFileOutput(PLAYER, Context.MODE_PRIVATE);;
			oos = new ObjectOutputStream(fos);
			oos.writeObject(CurrentPlayer.getPlayer());
			oos.close();		
		} 
		catch (FileNotFoundException e) {e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();}
	}

	public void getCurrentPlayer() 
	{
		/*FileInputStream input = null;   
		StringBuffer lu = new StringBuffer();
		try {
			int value;
			input = context.openFileInput(PLAYER);
			while((value = input.read()) != -1) {
				lu.append((char)value);
			}
			if(input != null)
				input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String mot = lu.toString();
		String[] tablePlayer = new String[11];
		int j = 0;
		for(int i = 0 ; i < mot.length() ; i++)
		{
			if (mot.charAt(i) == '/')
				j++;
			else
			{
				if (tablePlayer[j] == null)
					tablePlayer[j] = "";
				tablePlayer[j] += mot.charAt(i); 
			}
		}
		Integer[] tableIntPlayer = new Integer[10];
		for (int i = 0 ; i < 10 ; i++)
			tableIntPlayer[i] = Integer.parseInt((tablePlayer[i+1]));
		CurrentPlayer.createPlayer(tablePlayer[0], tableIntPlayer[0], tableIntPlayer[1], tableIntPlayer[2], tableIntPlayer[3], tableIntPlayer[4], tableIntPlayer[5], tableIntPlayer[6], tableIntPlayer[7], tableIntPlayer[8], tableIntPlayer[9] );
		*/
		Player player = null;
		try
		{
			FileInputStream fis = context.openFileInput(PLAYER);
			ObjectInputStream ois = new ObjectInputStream(fis);		
			player = (Player) ois.readObject();
			ois.close();
		}
		catch (FileNotFoundException e) {e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();} 
		catch (ClassNotFoundException e) {e.printStackTrace();}
		CurrentPlayer.setPlayer(player);
	}
}
