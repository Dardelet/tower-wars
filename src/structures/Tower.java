package structures;

import java.io.Serializable;


public class Tower  // Classe représentant une tour
	implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String tower; // "ARADRADAR" par exemple
	private int id;
	private double posX; // Position GPS de la tour
	private double posY; 
	private boolean isPosed;
	private Player player; 	// Player auquel appartient la Tower
	
	public Tower(String tower, int id, double posX, double posY, boolean isPosed, Player player)
	{
		this.tower = tower;
		this.id = id;
		this.posX = posX;
		this.posY = posY;
		this.isPosed = isPosed;
		this.player = player;
	}
	
	/*public Tower(Player player, String tower)
		throws HeightException
	{ 
		int n = tower.length();
		// On a donné en argument une liste de bloc
		// Si sa taille est supérieux au nombre de bloc permis
		if (n > player.calculateMaxHeight())
			throw new HeightException(); // Alors on renvoie une exception
		 
		this.player = player;
		this.posX = player.getPosX(); // La tour est construite sur la position du joueur
		this.posY = player.getPosY(); // -> Il faut donc se déplacer pour construire une tour
		this.tower = tower;
		
	}*/
	
	public Tower(String id, String blocs, String longitude, String latitude, boolean isPosed,String idOwner, String hauteur) 
	{
		this.tower = blocs;
		this.id = Integer.parseInt(id);
		this.posX = Integer.parseInt(latitude);
		this.posY = Integer.parseInt(longitude);
		this.isPosed = isPosed;
		Player player = new Player();
		player.setPlayerID(Integer.parseInt(idOwner));
		this.player = player;
	}

	public String toString()
	{
		return tower;
	}

	public String getTower() {
		return tower;
	}

	public void setTower(String tower) {
		this.tower = tower;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public boolean getIsPosed() 
	{
		return isPosed;
	}

	public void setIsPosed(boolean isPosed) {
		this.isPosed= isPosed;
	}



}