package structures;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import android.content.Context;

public class Towers // gère toutes les sauvegardes en mémoire de la classe CurrentTower
{
	private Context context;
	private static String TOWER = "tower.txt";

	public Towers(Context context)
	{
		this.context = context;
	}
	
	public void save(CurrentTowers currentTowers)
	{
		FileOutputStream fos;
		ObjectOutputStream oos;
		try 
		{
			fos = context.openFileOutput(TOWER, Context.MODE_PRIVATE);;
			oos = new ObjectOutputStream(fos);
			oos.writeObject(currentTowers);
			oos.close();		
		} 
		catch (FileNotFoundException e) {e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();}
	}
	
	public CurrentTowers load()
	{
		CurrentTowers currentTowers = null;
		try
		{
			FileInputStream fis = context.openFileInput(TOWER);
			ObjectInputStream ois = new ObjectInputStream(fis);		
			currentTowers = (CurrentTowers) ois.readObject();
			ois.close();
		}
		catch (FileNotFoundException e) {e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();} 
		catch (ClassNotFoundException e) {e.printStackTrace();}
		
		return currentTowers;			
	}
	
}
