package ununsedClasses;

import structures.Player;



public class ABloc // Bloc générique 'Attaque'
	extends Bloc
{
	public ABloc(Player player)
	{
		super(player);
	}
	
	public String getLabel()
	{
		return "A";
	}
}
