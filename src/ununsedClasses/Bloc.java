package ununsedClasses;

import structures.Player;


public abstract class Bloc // Bloc constituant une tour
{
	private Player player; // Le niveau d'amélioration de chaque bloc est spécifique au joueur
	
	public Bloc (Player player)
	{
		this.player = player;
	}
	
	public abstract String getLabel();
	
}
