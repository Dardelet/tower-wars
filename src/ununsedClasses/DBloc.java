package ununsedClasses;

import structures.Player;



public class DBloc // Bloc générique 'Défense'
	extends Bloc
{
	public DBloc(Player player)
	{
		super(player);
	}
	
	public String getLabel()
	{
		return "D";
	}
}
