package ununsedClasses;

import main.MenuActivity;

import com.pact.towerwars.R;
import com.pact.towerwars.R.id;
import com.pact.towerwars.R.layout;
import com.pact.towerwars.R.menu;

import serveur.ServerThread;
import structures.CurrentPlayer;
import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NewPlayerActivity extends Activity 
implements TextWatcher, View.OnClickListener

{
	private EditText name;
	private TextView warning;
	private Button create;
	private String playerName;
	private static int MENU_ACTIVITY = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_player);

		name = (EditText) findViewById(R.id.player_name);
		warning = (TextView) findViewById(R.id.warning);
		create = (Button) findViewById(R.id.create);


		name.addTextChangedListener(this);
		create.setVisibility(View.INVISIBLE); // Rend le bouton invisible tant que l'on a pas rentré de texte
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.new_player, menu);
		return true;
	}

	@Override
	public void afterTextChanged(Editable s) 
	{ // Après un changement du text du TextEdit
		warning.setText("Tu peux maintenant créer ton profil"); // Le warning change
		playerName = name.getText().toString(); //On enregistre le nom qui vient d'etre entré
		create.setVisibility(View.VISIBLE); // On rend le bouton visible
		create.setOnClickListener(this); // On active le click sur le bouton
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) 
	{

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) 
	{

	}

	@Override
	public void onClick(View v) 
	{
		saveInPhoneMemory();
		Intent menuIntent = new Intent(this, MenuActivity.class);
		startActivityForResult(menuIntent, MENU_ACTIVITY); // On va dans le menu
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{	
		if (requestCode == MENU_ACTIVITY) 
		{
			if (resultCode == RESULT_OK)  // Si on se déconnecte à partir du menu, on termine cette activité pour revenir directement à l'écran d'accueil
				finish();
		} // On affiche un message de provenance
	}


	private void saveInPhoneMemory () 
	{
		CurrentPlayer.initialiser(playerName); // Initialise un profil de nouveau joueur
		int id = sendPlayerToServer(playerName); // Récupère l'id du joueur depuis le serveur.
		CurrentPlayer.playerID = id;
		CurrentPlayer.save(this); // On sauve les infos du joueur
		//(new Players(this)).initialiserPlayers(playerName, id);// Initialise la correspondance, Id / Nom de Joueur
	}


	private int sendPlayerToServer(String playerName)
	{
		ServerThread server = new ServerThread(this);
		server.send(1, playerName);// 1 Correspond à l'instruction "nouveauJoueur"
		int id = -1;
		id = Integer.parseInt(server.takeANap());
		return id;
	}

	public void popUp(String message) 
	{
		Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
	}

}
