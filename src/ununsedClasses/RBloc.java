package ununsedClasses;

import structures.Player;



public class RBloc // Bloc générique 'Ressource'
	extends Bloc
{
	public RBloc(Player player)
	{
		super(player);
	}
	
	public String getLabel()
	{
		return "R";
	}
}
