package ununsedClasses;
import java.util.Hashtable;
import java.util.ArrayList;

import structures.Player;
import structures.Tower;


public class TowerStorage extends Hashtable<Player,ArrayList<Tower>>
{
	private static final long serialVersionUID = 1L;

	public ArrayList<Tower> getTowers(Player player)
	// Retourne la liste des tours que possède un joueur
	{
		return get(player);
	}
	
	public void addTowerToPlayerAccount(Player player, Tower tower)
	// Ajoute à la liste des tours que possède un joueur une nouvelle tour
	{
		ArrayList<Tower> newList = get(player);
		if (newList == null)
		{
			newList = new ArrayList<Tower>();
			newList.add(tower);
		}
		else	
			newList.add(tower);
		put(player, newList);
	}
	
	public void removeTowerFromPlayerAccount(Player player, Tower tower) // Non testée
	{// Supprime une tour à la liste des tours que possède un joueur
	// Aucun action sur la liste n'est effectuée si la tour se ne trouve pas dans la liste
		ArrayList<Tower> newList = get(player);
		if (newList != null)
			newList.remove(tower);
		put(player, newList);
	}
}
